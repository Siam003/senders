<aside id="sidebar" class="sidebar">
    <ul class="sidebar-nav" id="sidebar-nav">
        <li class="nav-item">
            <a class="nav-link collapsed" href="{{url('admin/dashboard')}}">
                <i class="bi bi-grid"></i>
                <span>Dashboard</span>
            </a>
        </li>
        <hr>
        <li class="nav-heading">Visitors</li>
        <li class="nav-item">
            <a href="{{url('admin/visitors/list')}}" class="nav-link {{request()->segment(2) == 'visitors' && request()->segment(3) == 'list' ? '' : 'collapsed'}}">
                <i class="bi bi-grid"></i><span>Visitor List</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{url('admin/visitors/create')}}" class="nav-link {{request()->segment(2) == 'visitors' && request()->segment(3) == 'create' ? '' : 'collapsed'}}">
                <i class="bi bi-grid"></i><span>Create Visitor</span>
            </a>
        </li>
        <hr>
        <li class="nav-heading">Schedule</li>
        <li class="nav-item">
            <a href="{{url('admin/schedules/list')}}" class="nav-link {{request()->segment(2) == 'schedules' && request()->segment(3) == 'list' ? '' : 'collapsed'}}">
                <i class="bi bi-grid"></i><span>Schedule List</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{url('admin/schedules/create')}}" class="nav-link {{request()->segment(2) == 'schedules' && request()->segment(3) == 'create' ? '' : 'collapsed'}}">
                <i class="bi bi-grid"></i><span>Create Schedule</span>
            </a>
        </li>

        <hr>
        <li class="nav-heading">Review & Approve & Reject</li>
        @if(Auth::user()->user_type == 2 || Auth::user()->user_type == 0)
        <li class="nav-item">
            <a class="nav-link {{request()->segment(2) == 'schedules' && request()->segment(3) == 'pending-review' ? '' : 'collapsed'}}" href="{{url('admin/schedules/pending-review')}}">
                <i class="bi bi-grid"></i>
                <span>Pending Review List</span>
            </a>
        </li>
        @endif
        @if(Auth::user()->user_type == 1 || Auth::user()->user_type == 0)
        <li class="nav-item">
            <a class="nav-link {{request()->segment(2) == 'schedules' && request()->segment(3) == 'pending-approval' ? '' : 'collapsed'}}" href="{{url('admin/schedules/pending-approval')}}">
                <i class="bi bi-grid"></i>
                <span>Pending Approval List</span>
            </a>
        </li>
        @endif

        <li class="nav-item">
            <a class="nav-link {{request()->segment(2) == 'schedules' && request()->segment(3) == 'approved-list' ? '' : 'collapsed'}}" href="{{url('admin/schedules/approved-list')}}">
                <i class="bi bi-grid"></i>
                <span> Approved List</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link {{request()->segment(2) == 'schedules' && request()->segment(3) == 'rejected-list' ? '' : 'collapsed'}}" href="{{url('admin/schedules/rejected-list')}}">
                <i class="bi bi-grid"></i>
                <span> Rejected List</span>
            </a>
        </li>
        <hr>
        @if(Auth::user()->user_type != 1)
        <li class="nav-heading">Reports</li>
        <li class="nav-item">
            <a class="nav-link collapsed" href="{{url('admin/visitor-history')}}">
                <i class="bi bi-person"></i>
                <span>Visitor History</span>
            </a>
        </li>
        @endif

        <hr>
        @if(Auth::user()->user_type == 0)
        <li class="nav-heading">Settings</li>
        <li class="nav-item">
            <a class="nav-link collapsed" href="{{url('admin/users')}}">
                <i class="bi bi-person"></i>
                <span>Users</span>
            </a>
        </li>
        @endif
    </ul>
</aside>
