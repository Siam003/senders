@php
$userType = Auth::user()->user_type;
$userDepartment = Auth::user()->department_id;

$allPendingReviewSchedule = 0;
if ($userType == 2) {
$allSameDeptEmployees = \App\Employee::where('department_id', $userDepartment)->pluck('id');
$query = \App\VisitorSchedule::where('status', \App\VisitorSchedule::CREATED_STATUS)->whereIn('visit_with', $allSameDeptEmployees);;
$allPendingReviewSchedule = $query->count();
}

if($userType == 1){
$allSameDeptEmployees = \App\Employee::where('department_id', $userDepartment)->pluck('id');
$query = \App\VisitorSchedule::where('status', \App\VisitorSchedule::REVIEWD_STATUS)->whereIn('visit_with', $allSameDeptEmployees);;
$allPendingReviewSchedule = $query->count();
}

$notification = $allPendingReviewSchedule;

@endphp

<header id="header" class="header fixed-top d-flex align-items-center">
    <div class="d-flex align-items-center justify-content-between">
        <a href="{{url('admin/dashboard')}}" class="logo d-flex align-items-center">
            <img src="assets/img/logo.png" alt="">
            <span class="d-none d-lg-block">Visitor Management</span>
        </a>
        <i class="bi bi-list toggle-sidebar-btn"></i>
    </div><!-- End Logo -->

    <nav class="header-nav ms-auto">
        <ul class="d-flex align-items-center">

            <li class="nav-item d-block d-lg-none">
                <a class="nav-link nav-icon search-bar-toggle " href="#">
                    <i class="bi bi-search"></i>
                </a>
            </li><!-- End Search Icon-->

            <li class="nav-item dropdown">

                <a class="nav-link nav-icon" href="#" data-bs-toggle="dropdown">
                    <i class="bi bi-bell"></i>
                    <span class="badge bg-primary badge-number">{{$notification}}</span>
                </a><!-- End Notification Icon -->

                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow notifications">
                    <li class="dropdown-header">
                        You have {{$notification}} new notifications!
                        @if($userType == 1)
                        <a href="{{url('admin/schedules/pending-approval')}}"><span class="badge rounded-pill bg-primary p-2 ms-2">View all</span></a>
                        @endif

                        @if($userType == 2)
                        <a href="{{url('admin/schedules/pending-review')}}"><span class="badge rounded-pill bg-primary p-2 ms-2">View all</span></a>
                        @endif
                    </li>
                </ul><!-- End Notification Dropdown Items -->

            </li><!-- End Notification Nav -->

            <li class="nav-item dropdown pe-3">

                <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
                    <img src="{{asset('assets/img/profile-img.jpg')}}" alt="Profile" class="rounded-circle">
                    <span class="d-none d-md-block dropdown-toggle ps-2">{{Auth::user()->name}}</span>
                </a><!-- End Profile Iamge Icon -->

                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                    <li class="dropdown-header">
                        <h6>{{Auth::user()->user_name}}</h6>
                        <span>{{\App\User::DESIGNATION[Auth::user()->designation_id]}} ( {{\App\User::USER_TYPE[Auth::user()->user_type]}} )</span>
                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li>
                        <a class="dropdown-item d-flex align-items-center" href="{{url('admin/logout')}}">
                            <i class="bi bi-box-arrow-right"></i>
                            <span>Sign Out</span>
                        </a>
                    </li>

                </ul><!-- End Profile Dropdown Items -->
            </li><!-- End Profile Nav -->

        </ul>
    </nav><!-- End Icons Navigation -->

</header>ß
