@extends('layouts.application', [])
@section('content')
<div class="pagetitle">
    <h1>Application Dashboard</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
        </ol>
    </nav>
</div>
<section class="section dashboard">
    <div class="row">

        <!-- Left side columns -->
        <div class="col-lg-8">
            <div class="row">

                <!-- Sales Card -->
                <div class="col-xxl-4 col-md-6">
                    <div class="card info-card sales-card">
                        <div class="card-body">
                            <h5 class="card-title">Scheduled <span>| Today</span></h5>
                            <div class="d-flex align-items-center">
                                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                    <i class="bi bi-people"></i>
                                </div>
                                <div class="ps-3">
                                    <h6>{{$total_schedule}}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Revenue Card -->
                <div class="col-xxl-4 col-md-6">
                    <div class="card info-card revenue-card">
                        <div class="card-body">
                            <h5 class="card-title">In Status <span>| Today</span></h5>
                            <div class="d-flex align-items-center">
                                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                    <i class="bi bi-people"></i>
                                </div>
                                <div class="ps-3">
                                    <h6>{{$todays_total_in}}</h6>
                                </div>
                            </div>
                        </div>

                    </div>
                </div><!-- End Revenue Card -->

                <!-- Customers Card -->
                <div class="col-xxl-4 col-xl-12">
                    <div class="card info-card customers-card">
                        <div class="card-body">
                            <h5 class="card-title">Visit Complete <span>| Today</span></h5>
                            <div class="d-flex align-items-center">
                                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                    <i class="bi bi-people"></i>
                                </div>
                                <div class="ps-3">
                                    <h6>{{$todays_total_out}}</h6>
                                </div>
                            </div>

                        </div>
                    </div>

                </div><!-- End Customers Card -->


            </div>
        </div><!-- End Left side columns -->

    </div>
</section>
<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Schedule List</h5>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="text-center">
                                    <th>Schedule No</th>
                                    <th>Visitor</th>
                                    <th>Image</th>
                                    <th>Visit With</th>
                                    <th>Purpose</th>
                                    <th>Schedule Time</th>
                                    <th>Status</th>
                                    <th>Visit Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            @if(isset($todaysSchedule) && count($todaysSchedule) > 0)
                            @foreach($todaysSchedule as $key => $value)
                            <tr class="text-center">
                                <td>{{$value->schedule_no}}</td>
                                <td>{{$value->visitor->first_name}}</td>
                                <td>
                                    @if($value->visitor->photo)
                                    <img class="img-thumbnail" style="width:50px;height:50px" src="{{ $value->visitor->photo }}">
                                    @else
                                    <a class="align-items-center error" href="{{url('admin/visitors/edit/'.$value->visitor->id)}}">
                                        <span><u>Click to add image</u></span>
                                    </a>
                                    @endif

                                </td>
                                <td>{{$value->visitEmployee->first_name}}</td>
                                <td>{{$value->purpose == 1 ? 'Official' : 'Personal'}}</td>
                                <td>{{$value->scheduled_time}}</td>
                                <td>{!! $value->status!!}</td>
                                <td>
                                    @if($value->exit_time == null && $value->entry_time != null)
                                    Visit Ongoing
                                    @endif
                                    @if($value->exit_time == null && $value->entry_time == null)
                                    Visit Pending
                                    @endif
                                    @if($value->exit_time != null && $value->entry_time != null)
                                    Visit Complete
                                    @endif
                                </td>
                                <td>
                                    <ul class="d-flex align-items-center" style="list-style: none;">
                                        <li class="nav-item dropdown pe-3">
                                            <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
                                                <span class="d-none d-md-block dropdown-toggle ps-2">Action</span>
                                            </a><!-- End Profile Iamge Icon -->
                                            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                                                @if($value->exit_time == null && $value->entry_time == null)
                                                <li>
                                                    <a class="dropdown-item d-flex align-items-center" href="{{url('admin/schedules/checkin?schedule-id='.$value->id)}}">
                                                        <i class="bi bi-pencil-square"></i>
                                                        <span>Check In</span>
                                                    </a>
                                                </li>
                                                @endif
                                                @if($value->exit_time == null && $value->entry_time != null)
                                                <li>
                                                    <a class="dropdown-item d-flex align-items-center" href="{{url('admin/schedules/checkout?schedule-id='.$value->id)}}">
                                                        <i class="bi bi-pencil-square"></i>
                                                        <span>Check Out</span>
                                                    </a>
                                                </li>
                                                @endif
                                                <li>
                                                    <a class="dropdown-item d-flex align-items-center" href="{{url('admin/schedule/details?schedule-id='.$value->id)}}">
                                                        <i class="bi bi-card-checklist"></i>
                                                        <span>Details</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="dropdown-item d-flex align-items-center" href="{{url('admin/schedule/occurance-entry?schedule-id='.$value->id)}}">
                                                        <i class="bi bi-cone-striped"></i>
                                                        <span>Occurance Entry</span>
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>

                                    </ul>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
