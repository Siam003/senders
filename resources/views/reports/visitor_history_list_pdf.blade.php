<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>
        body {
            font-size: 12px;
        }

        table thead tr th {
            border: 1px solid #ccc;
        }

        table tbody tr td {
            border: 1px solid #ccc;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        .header-div {
            width: 100%;
            text-align: center;
        }

    </style>
</head>
<body>
    <div class="header-div">
        <h3> Visitor History </h3>
        <p>Report Generate Time : {{date('d M Y h:i')}}</p>
    </div>

    <div class="generate-parameter">
        @php
        $string = '';
        foreach($search_array as $key => $value){
        if($key == 'status' && $value != null){
        $value = $value ? \App\VisitorSchedule::STATUS[$value] : '';
        $string .= str_replace('_', ' ',ucwords($key)) . ' : ' .ucwords($value) .', ';
        }
        if($key == 'department_id' && $value != null){
        $value = $value ? \App\User::DEPARTMENTS[$value] : '';
        $string .= str_replace('_', ' ',ucwords($key)) . ' : ' .ucwords($value) .', ';
        }
        if($key == 'from_date' && $value != null){
        $string .= str_replace('_', ' ',ucwords($key)) . ' : ' .ucwords($value) .', ';
        }
        if($key == 'to_date' && $value != null){
        $string .= str_replace('_', ' ',ucwords($key)) . ' : ' .ucwords($value) .', ';
        }
        if($key == 'mobile_no' && $value != null){
        $string .= str_replace('_', ' ',ucwords($key)) . ' : ' .ucwords($value) .', ';
        }
        if($key == 'name' && $value != null){
        $string .= str_replace('_', ' ',ucwords($key)) . ' : ' .ucwords($value) .', ';
        }
        if($key == 'visitor_company' && $value != null){
        $string .= str_replace('_', ' ',ucwords($key)) . ' : ' .ucwords($value) .', ';
        }

        }
        @endphp
        <p>Report Generate with param list : {{$string}}</p>
    </div>

    <hr>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Schedule No</th>
                <th>Visitor</th>
                <th>Mobile No</th>
                <th>Company</th>
                <th>Visit To</th>
                <th>Purpose</th>
                <th>Date</th>
                <th>Time</th>
                <th>Status</th>
                <th>Material</th>
            </tr>
        </thead>
        <tbody>
            @foreach($schedules as $key => $model)
            <tr>
                <td>{{$model->schedule_no}}</td>
                <td>{{$model->visitor->first_name}}</td>
                <td>{{$model->visitor->mobile_no}}</td>
                <td>{{$model->visitor->visitor_company}}</td>
                <td>{{$model->visitEmployee->first_name}}</td>
                <td>{{$model->purpose == 1 ? 'Official' : 'Personal'}}</td>
                <td>{{$model->visit_date}}</td>
                <td>{{$model->scheduled_time}}</td>
                <td>{!! $model->status !!}</td>
                <td>{{$model->has_material == 1 ? 'Yes' : 'No'}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
