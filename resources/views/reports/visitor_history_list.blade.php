@extends('layouts.application', [])
@section('content')
<div class="pagetitle">
    <h1>Visitor History</h1>
</div>


<section class="section">
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <br>
                    <form action="{{url('admin/visitor-history')}}" method="get" class="row row-cols-lg-auto g-3 align-items-center">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>From Date</th>
                                        <th>To Date</th>
                                        <th>Visitor Name</th>
                                        <th>Visitor Company</th>
                                        <th>Department</th>
                                        <th>Mobile No</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="width:10%">{{Form::date('from_date', old('from_date'), ['class' =>'form-control'])}}</td>
                                        <td style="width:10%">{{Form::date('to_date', old('to_date'), ['class' =>'form-control'])}}</td>
                                        <td style="width:15%">{{Form::text('name',$search_array['name'] ??old('name'), ['class' =>  'form-control'])}}</td>
                                        <td style="width:15%">{{Form::select('visitor_company',$visitorsCompany, $search_array['visitor_company'] ?? old('visitor_company'),['class' =>  'form-select','id'=>'visitor_id','placeholder'=>'Select'])}}</td>
                                        <td style="width:12%">{{Form::select('department_id',$visitorsDepartment, $search_array['department_id'] ?? old('department_id'),['class' =>  'form-select','id'=>'department_id','placeholder'=>'Select'])}}</td>
                                        <td style="width:10%">{{Form::text('mobile_no',$search_array['mobile_no'] ??old('mobile_no'), ['class' =>  'form-control'])}}</td>
                                        <td style="width:10%">{{Form::select('status',$status,$search_array['status'] ??old('mobile_no'), ['class' =>  'form-select','id'=>'status','placeholder'=>'Select'])}}</td>
                                        <td>
                                            <button type="submit" class="btn btn-primary"><i class="bi bi-search"></i></button>
                                            <a href="{{url('admin/visitor-history')}}" class="btn btn-warning"><i class="bi bi-power"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row" style="margin-top: 10px;margin-bottom:10px">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="float-start">
                        Search Results
                    </div>
                    <div class="float-end">
                        <a href="{{url('admin/visitor-history-list-download?'.Request::getQueryString())}}" class="btn btn-warning" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Download PDF"><i class="bi bi-file-earmark-pdf"></i></a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <br>
                        @if(!empty($schedules))
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Schedule No</th>
                                    <th>Visitor</th>
                                    <th>Mobile No</th>
                                    <th>Company</th>
                                    <th>Visit To</th>
                                    <th>Purpose</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Status</th>
                                    <th>Material</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($schedules as $key => $model)
                                <tr>
                                    <td>{{$model->schedule_no}}</td>
                                    <td>{{$model->visitor->first_name}}</td>
                                    <td>{{$model->visitor->mobile_no}}</td>
                                    <td>{{$model->visitor->visitor_company}}</td>
                                    <td>{{$model->visitEmployee->first_name}}</td>
                                    <td>{{$model->purpose == 1 ? 'Official' : 'Personal'}}</td>
                                    <td>{{$model->visit_date}}</td>
                                    <td>{{$model->scheduled_time}}</td>
                                    <td>{!! $model->status !!}</td>
                                    <td>{{$model->has_material == 1 ? 'Yes' : 'No'}}</td>
                                    <td>
                                        <a href="{{url('admin/schedule/details?schedule-id='.$model->id)}}" class="btn btn-sm btn-primary" data-bs-toggle="tooltip" data-bs-placement="bottom" title="view details"><i class="bi bi-eye"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination">
                            {{ $schedules->appends($_GET)->links() }}
                        </div>
                        @else
                        <p class="text-center">No data to show</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
