@extends('layouts.application', [])
@section('content')
<div class="pagetitle">
    <h1>Visitor History</h1>
</div>

<section class="section">

    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <br>
                    <form action="{{url('admin/visitor-history-search')}}" method="get" class="row row-cols-lg-auto g-3 align-items-center">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Name</th>
                                    <th>Company</th>
                                    <th>Department</th>
                                    <th>Mobile No</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="width:10%">{{Form::date('from_date', old('from_date'), ['class' =>'form-control'])}}</td>
                                    <td style="width:10%">{{Form::date('to_date', old('to_date'), ['class' =>'form-control'])}}</td>
                                    <td style="width:15%">{{Form::text('name',$search_array['name'] ??old('name'), ['class' =>  'form-control'])}}</td>
                                    <td style="width:15%">{{Form::select('visitor_company',$visitorsCompany, $search_array['visitor_company'] ?? old('visitor_company'),['class' =>  'form-select','id'=>'visitor_id','placeholder'=>'Select'])}}</td>
                                    <td style="width:15%">{{Form::select('department_id',$visitorsDepartment, $search_array['department_id'] ?? old('department_id'),['class' =>  'form-select','id'=>'department_id','placeholder'=>'Select'])}}</td>
                                    <td style="width:15%">{{Form::text('mobile_no',$search_array['mobile_no'] ??old('mobile_no'), ['class' =>  'form-control'])}}</td>
                                    <td>
                                        <button type="submit" class="btn btn-primary"><i class="bi bi-search"></i></button>
                                        <a href="{{url('admin/visitor-history')}}" class="btn btn-warning"><i class="bi bi-trash"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row" style="margin-top: 10px;margin-bottom:10px">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">Visitor Full History</div>
                <div class="card-body">
                    @if(!empty($schedules))
                    <br>
                    <p>Visitor Personal Information</p>
                    <br>
                    <table class="table table-bordered">
                        <thead>
                            <tr class="text-center">
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Company</th>
                                <th scope="col">Designation</th>
                                <th scope="col">Email</th>
                                <th scope="col">Mobile No</th>
                                <th scope="col">NID</th>
                                <th scope="col">Photo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="text-center">
                                <td>{{$schedules->first_name}}</td>
                                <td>{{$schedules->last_name}}</td>
                                <td>{{$schedules->visitor_company}}</td>
                                <td>{{$schedules->visitor_designation}}</td>
                                <td>{{$schedules->email}}</td>
                                <td>{{$schedules->mobile_no}}</td>
                                <td>{{$schedules->nid}}</td>
                                <td><img class="img-thumbnail" style="width:50px;height:50px" src="{{ $schedules->photo ? $schedules->photo : asset('/uploads/user/avatar.png') }}"></td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                    @else
                    <p class="text-center m-2">No Data To Display</p>
                    @endif
                </div>
            </div>
            @if(!empty($schedules->schedules))
            @foreach($schedules->schedules as $key => $model)
            <div class="card">
                <div class="card-header">
                    Visitor Full History
                </div>
                <div class="card-body">
                    <br>
                    <table class="table table-bordered">
                        <tr>
                            <th>Schedule No</th>
                            <th>Visit With</th>
                            <th>Purpose</th>
                            <th>Visit Date</th>
                            <th>Schedule Time</th>
                            <th>Status</th>
                            <th>Material</th>
                            <th>Vehical</th>
                            <th>Created By</th>
                            <th>Reviewd By</th>
                            <th>Approved By</th>
                        </tr>
                        <tr>
                            <td>{{$model->schedule_no}}</td>
                            <td>{{$model->visitEmployee->first_name}}</td>
                            <td>{{$model->purpose == 1 ? 'Official' : 'Personal'}}</td>
                            <td>{{$model->visit_date}}</td>
                            <td>{{$model->scheduled_time}}</td>
                            <td>{!! $model->status !!}</td>
                            <td>{{$model->has_material == 1 ? 'Yes' : 'No'}}</td>
                            <td>{{$model->has_vehical == 1 ? 'Yes' : 'No'}}</td>
                            <td>{{$model->createdBy->name}}</td>
                            <td>{{$model->reviewedBy->name ?? 'Pending'}}</td>
                            <td>{{$model->approvedBy->name ?? 'Pending'}}</td>
                        </tr>
                    </table>
                    <p>Visitor Material</p>
                    <div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Material</th>
                                    <th>Serial No</th>
                                    <th>Description</th>
                                    <th>Quantity</th>
                                    <th>Returnable</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($model->materials) && count($model->materials) > 0)
                                @foreach($model->materials as $key=>$value)
                                <tr>
                                    <td>{{$value->material_name}}</td>
                                    <td>{{$value->serial_no}}</td>
                                    <td>{{$value->description}}</td>
                                    <td>{{$value->quantity}}</td>
                                    <td>{{$value->is_returnable == 1 ? 'Yes' : 'No'}}</td>
                                    <td>{{$value->remarks}}</td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="6">
                                        <p class="text-center m-2">No Material To Display</p>
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <hr>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>
@endsection
