@extends('layouts.application', [])
@section('content')
<div class="pagetitle">
    <h1>Occurance Entry</h1>
</div>

<section class="section">
    <div style="margin-top: 10px;margin-bottom:10px">
        <a href="{{url('admin/visitors/list')}}" class="btn btn-primary"><i class="bi bi-arrow-left-circle-fill"></i> Back</a>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h5 style="margin:0px">Occurance Information</h5>
                </div>
                <div class="card-body">
                    <!-- General Form Elements -->
                    {!! Form::model(['url' =>'admin/ocurance/store', 'method' => 'POST','autocomplete' => 'off', 'onsubmit' => 'submit.disabled = true; return true;','class'=>'row g-3','novalidate'=>'novalidate','files'=>true]) !!}
                    <br>
                    <input type="hidden" name="schedule_id" value="{{$schedule_id}}">
                    <div class="row mb-3">
                        <label for="inputText" class="col-sm-2 col-form-label">Ocurance Type</label>
                        <div class="col-sm-10">
                            {{Form::select('occurance_type',['theft'=>'Theft','broken'=>'Broken','others'=>'Others'],old('occurance_type'), ['class' => 'form-select','id'=>'occurance_type','placeholder'=>'Select Occurance Type'])}}
                            @if($errors->has('occurance_type'))
                            <div class="error">{{ $errors->first('occurance_type') }}</div>
                            @endif

                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Occurance Details</label>
                        <div class="col-sm-10">
                            {{Form::textarea('occurance_details', old('occurance_details'), ['class' => 'form-control','id'=>'occurance_details'])}}
                            @if($errors->has('occurance_details'))
                            <div class="error">{{ $errors->first('occurance_details') }}</div>
                            @endif

                        </div>
                    </div>
                    <hr style="margin-top:40px">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Save</button>
                        <button type="reset" class="btn btn-danger">Cancel</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
</section>
@endsection

@push('custom-scripts')
<script>
    $(function() {

        $('#has_material').on('change', function() {
            var has_material = $(this).val();
            if (has_material == 1) {
                $('table > thead > tr').removeClass('row-disabled');
                $('table > tbody > tr').removeClass('row-disabled');
            } else {
                $('table > thead > tr').addClass('row-disabled');
                $('table > tbody > tr').addClass('row-disabled');
            }
        });
        $('#has_vahical').on('change', function() {
            var has_vahical = $(this).val();
            if (has_vahical == 1) {
                $('.vehical_no_div').removeClass('d-none');
            } else {
                $('.vehical_no_div').addClass('d-none');
            }
        });

        $('body').on('click', '.add', function() {
            var clone_row = $(this).parents('tr').clone();
            clone_row.find('.remove').removeClass('d-none');
            $(this).parents('tr').after(clone_row);
        });
        $('body').on('click', '.remove', function() {
            $(this).parents('tr').remove();
        });

        $('.submit_button_type').on('click', function() {
            var clickedType = $(this).attr('clicked_val');
            $('#submit_type').val(clickedType);
        });



    });

</script>
@endpush
