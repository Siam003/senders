@extends('layouts.application', [])
@section('content')
<div class="pagetitle">
    <h1>{{$model ? 'Update User' : 'Create User' }}</h1>
</div>

<section class="section">
    <div style="margin-top: 10px;margin-bottom:10px">
        <a href="{{url('admin/users')}}" class="btn btn-primary"><i class="bi bi-arrow-left-circle-fill"></i> Back</a>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">Users Table</div>
                <div class="card-body">
                    <br>
                    {!! Form::model($model,['url' => $model ? 'admin/users/'.$model->id.'/update' : 'admin/users/store', 'method' => $model ? 'PUT' : 'POST','autocomplete' => 'off', 'onsubmit' => 'submit.disabled = true; return true;','class'=>'row g-3','novalidate'=>'novalidate','files'=>true]) !!}
                    <div class="col-md-4">
                        <label for="name" class="form-label"> Name</label>
                        {{Form::text('name',null, ['class' => $errors->first('name') ? 'error-border form-control' : 'form-control','id'=>'name'])}}
                        @if($errors->has('name'))
                        <div class="error">{{ $errors->first('name') }}</div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <label for="inputEmail5" class="form-label">User Name</label>
                        {{Form::text('user_name',null, ['class' =>$errors->first('user_name') ? 'error-border form-control' : 'form-control','id'=>'name'])}}
                        @if($errors->has('user_name'))
                        <div class="error">{{ $errors->first('user_name') }}</div>
                        @endif
                    </div>
                    <div class="col-4">
                        <label for="official_id" class="form-label">Official Id</label>
                        {{Form::text('official_id',null, ['class' =>$errors->first('official_id') ? 'error-border form-control' : 'form-control','id'=>'official_id'])}}
                        @if($errors->has('official_id'))
                        <div class="error">{{ $errors->first('official_id') }}</div>
                        @endif
                    </div>
                    <div class="col-4">
                        <label for="department" class="form-label">Department</label>
                        {{Form::select('department_id',$departments,null, ['class' =>$errors->first('department_id') ? 'error-border form-control' : 'form-control','id'=>'department_id','placeholder'=>'Select Department'])}}
                        @if($errors->has('department_id'))
                        <div class="error">{{ $errors->first('department_id') }}</div>
                        @endif

                    </div>
                    <div class="col-md-4">
                        <label for="designation_id" class="form-label">Designation</label>
                        {{Form::select('designation_id',$designations,null, ['class' =>$errors->first('designation_id') ? 'error-border form-control' : 'form-control','id'=>'designation_id','placeholder'=>'Select Designation'])}}
                        @if($errors->has('designation_id'))
                        <div class="error">{{ $errors->first('designation_id') }}</div>
                        @endif

                    </div>
                    <div class="col-md-4">
                        <label for="user_type" class="form-label">User Type</label>
                        {{Form::select('user_type',$user_types,null, ['class' =>$errors->first('user_type') ? 'error-border form-control' : 'form-control','id'=>'user_type','placeholder'=>'Select User Type'])}}
                        @if($errors->has('user_type'))
                        <div class="error">{{ $errors->first('user_type') }}</div>
                        @endif

                    </div>
                    <div class="col-md-4">
                        <label for="email" class="form-label">Email</label>
                        {{Form::email('email',null, ['class' =>$errors->first('email') ? 'error-border form-control' : 'form-control','id'=>'email'])}}
                        @if($errors->has('email'))
                        <div class="error">{{ $errors->first('email') }}</div>
                        @endif

                    </div>
                    <div class="col-md-4">
                        <label for="mobile_no" class="form-label">Mobile No</label>
                        {{Form::text('mobile_no',null, ['class' =>$errors->first('mobile_no') ? 'error-border form-control' : 'form-control','id'=>'mobile_no'])}}
                        @if($errors->has('mobile_no'))
                        <div class="error">{{ $errors->first('mobile_no') }}</div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <label for="photo" class="form-label">Photo</label>
                        {{Form::file('photo', ['class' =>$errors->first('photo') ? 'error-border form-control' : 'form-control','id'=>'photo'])}}
                        @if($errors->has('photo'))
                        <div class="error">{{ $errors->first('photo') }}</div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <label for="check_pass_change" class="form-label">Create / Change Password ? </label>
                        {{Form::select('check_pass_change',['1'=>'No','2'=>'Yes'],old('check_pass_change'), ['class' => 'form-select','id'=>'check_pass_change','placeholder'=>'Select Option'])}}
                        @if($errors->has('check_pass_change'))
                        <div class="error">{{ $errors->first('check_pass_change') }}</div>
                        @endif
                    </div>

                    <div class="col-md-4">
                        <label for="password" class="form-label">Password</label>
                        {{Form::password('password', ['class' =>$errors->first('password') ? 'error-border form-control' : 'form-control','id'=>'password','disabled'=> old('check_pass_change') == 2 ? false : true])}}
                        @if($errors->has('password'))
                        <div class="error">{{ $errors->first('password') }}</div>
                        @endif
                    </div>
                    <div class="col-md-4 has-validation">
                        <label for="password_confirmation" class="form-label">Confirm Password</label>
                        {{Form::password('password_confirmation', ['class' => 'form-control','id'=>'password_confirmation','disabled'=> old('check_pass_change') == 2 ? false : true ])}}
                        @if($errors->has('password_confirmation'))
                        <div class="error">{{ $errors->first('password_confirmation') }}</div>
                        @endif
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn btn-secondary">Reset</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
</section>
@endsection


@push('custom-scripts')
<script>
    $(function() {


        $('#check_pass_change').on('change', function() {
            var check_pass_change = $('#check_pass_change').val();
            if (check_pass_change == '2') {
                $('#password').removeAttr('disabled')
                $('#password_confirmation').removeAttr('disabled')
            } else {
                $('#password').attr('disabled','disabled')
                $('#password_confirmation').attr('disabled','disabled')

            }

        });

    });

</script>
@endpush
