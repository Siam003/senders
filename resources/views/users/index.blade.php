@extends('layouts.application', [])
@section('content')
<div class="pagetitle">
    <h1>Users List</h1>
</div>

<section class="section">
    <div style="margin-top: 10px;margin-bottom:10px">
        <a href="{{url('admin/users/create')}}" class="btn btn-success"><i class="bi bi-plus"></i> Create User</a>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">Users Table</div>
                <div class="card-body">
                    <!-- Default Table -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">User Name</th>
                                    <th scope="col">Office Id</th>
                                    <th scope="col">Designation</th>
                                    <th scope="col">Department</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Mobile No</th>
                                    <th scope="col">User Type</th>
                                    <th scope="col">Photo</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->user_name}}</td>
                                    <td>{{$user->official_id}}</td>
                                    <td>{{\App\User::DESIGNATION[$user->designation_id]}}</td>
                                    <td>{{\App\User::DEPARTMENTS[$user->department_id]}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->mobile_no}}</td>
                                    <td>{{\App\User::USER_TYPE[$user->user_type]}}</td>
                                    <td><img class="img-thumbnail" style="width:50px;height:50px" src="{{ asset($user->photo ? $user->photo : '/uploads/user/avatar.png') }}"></td>
                                    <td>
                                        @if($user->id != 1)
                                        <a href="{{url('admin/users/edit/'.$user->id)}}" class="btn btn-sm btn-primary" data-bs-toggle="tooltip" data-bs-placement="bottom" title="edit"><i class="bi bi-pencil-square"></i></a>
                                        <a href="{{url('admin/users/delete/'.$user->id)}}" onclick="return confirm('Are you sure?')" class="btn btn-sm btn-danger" data-bs-toggle="tooltip" data-bs-placement="bottom" title="delete"><i class="bi bi-trash"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="text-center">{{ $users->appends($_GET)->links() }}</div>
                </div>

            </div>

        </div>
    </div>
</section>
@endsection
