@extends('layouts.application', [])
@section('content')
<div class="pagetitle">
    <h1>Approved Schedule List</h1>
</div>

<section class="section">
    <div class="">
        <form action="{{url('admin/schedules/approved-list')}}" method="get" class="row row-cols-lg-auto g-3 align-items-center">
            <div class="col-12">
                {{Form::select('visitor_id',$visitors,$search_array ? $search_array['visitor_id'] : '', ['class' =>'form-select','id'=>'visitor_id','placeholder'=>'Choose Visitor'])}}
            </div>
            <div class="col-12">
                {{Form::select('visit_with',$employees,$search_array ? $search_array['visit_with'] : '', ['class' =>'form-select','id'=>'visit_with','placeholder'=>'Choose Visit With'])}}
            </div>
            <div class="col-12">
                {{Form::select('status',$status,$search_array ? $search_array['status'] : '', ['class' =>'form-select','placeholder'=>'Choose Status'])}}
            </div>
            <div class="col-12">
                <div class="input-group">
                    {{Form::date('date',$search_array ? $search_array['date'] : '', ['class' =>  'form-control','placeholder'=>'Keyword'])}}
                </div>
            </div>
            <div class="col-12">
                <div class="input-group">
                    {{Form::text('schedule_no',$search_array ? $search_array['schedule_no'] : '', ['class' =>  'form-control','placeholder'=>'Schedule no'])}}
                </div>
            </div>
            <div class="col-12">
                <button type="submit" class="btn btn-primary"><i class="bi bi-search"></i> Search</button>
                <a href="{{url('admin/schedules/approved-list')}}" class="btn btn-warning"><i class="bi bi-trash"></i> Reset</a>
            </div>
        </form>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">Schedule Table</div>
                <div class="card-body">
                    <br>
                    <!-- Default Table -->
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr class="text-center">
                                <th>Schedule No</th>
                                <th>Visitor</th>
                                <th>Visit With</th>
                                <th>Purpose</th>
                                <th>Visit Date</th>
                                <th>Schedule Time</th>
                                <th>Entry </th>
                                <th>Exit</th>
                                <th>Status</th>
                                <th>Material</th>
                                <th>Vehical</th>
                                <th>Action</th>
                            </tr>
                            @if(isset($schedules) && count($schedules) > 0)
                            @foreach($schedules as $key => $value)
                            <tr class="text-center">
                                <td>{{$value->schedule_no}}</td>
                                <td>{{$value->visitor->first_name}}</td>
                                <td>{{$value->visitEmployee->first_name}}</td>
                                <td>{{$value->purpose == 1 ? 'Official' : 'Personal'}}</td>
                                <td>{{$value->visit_date}}</td>
                                <td>{{$value->scheduled_time}}</td>
                                <td>{{$value->entry_time}}</td>
                                <td>{{$value->exit_time}}</td>
                                <td>{!! $value->status!!}</td>
                                <td>{{$value->has_material == 1 ? 'Yes' : 'No'}}</td>
                                <td>{{$value->has_vehical == 1 ? 'Yes' : 'No'}}</td>
                                <td>
                                    <ul class="d-flex align-items-center" style="list-style: none;">
                                        <li class="nav-item dropdown pe-3">
                                            <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
                                                <span class="d-none d-md-block dropdown-toggle ps-2">Action</span>
                                            </a><!-- End Profile Iamge Icon -->
                                            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                                                <li>
                                                    <a class="dropdown-item d-flex align-items-center" href="{{url('admin/schedules/gatepass-details?schedule-id='.$value->id)}}">
                                                        <i class="bi bi-pencil-square"></i>
                                                        <span>View Gatepass</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </table>
                    </div>
                    <div class="pagination">
                        {{ $schedules->appends($_GET)->links() }}
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
@endsection
