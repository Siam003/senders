@extends('layouts.application', [])
@section('content')
<div class="pagetitle">
    <h1>Create Schedule</h1>
</div>

<section class="section">
    <div style="margin-top: 10px;margin-bottom:10px">
        <a href="{{url('admin/visitors/list')}}" class="btn btn-primary"><i class="bi bi-arrow-left-circle-fill"></i> Back</a>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <hr>
            <div class="card">
                <div class="card-header">
                    <h5 style="margin:0px">Schedule Information</h5>
                </div>
                <div class="card-body">
                    <br>
                    {!! Form::model($model,['url' => $model ? 'admin/schedule/'.$model->id.'/update' : 'admin/schedule/store', 'method' => $model ? 'PUT' : 'POST','autocomplete' => 'off', 'onsubmit' => 'submit.disabled = true; return true;','class'=>'row g-3','novalidate'=>'novalidate','files'=>true]) !!}
                    <input type="hidden" name="submit_type" id="submit_type">
                    <input type="hidden" name="schedule_id" value={{$model ? $model->id : null}}>
                    <input type="hidden" name="check_in" value={{request()->get('schedule-id')}}>
                    <div class="col-md-3">
                        <label for="schedule_no" class="form-label">Schedule No</label>
                        {{Form::text('schedule_no',$model->schedule_no ?? $schedule_no, ['class' =>  'form-control','id'=>'schedule_no','readonly'=>true])}}
                        @if($errors->has('schedule_no'))
                        <div class="error">{{ $errors->first('schedule_no') }}</div>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <label for="purpose" class="form-label">Visitor</label>
                        {{Form::select('visitor_id',$visitors,$visitor_id ?? old('purpose'), ['class' => 'form-select','id'=>'visitor_id','placeholder'=>'Select Visitor','readonly' => $visitor_id ? true : ''])}}
                        @if($errors->has('visitor_id'))
                        <div class="error">{{ $errors->first('visitor_id') }}</div>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <label for="purpose" class="form-label">Purpose</label>
                        {{Form::select('purpose',['1'=>'Procurement','2'=>'Bill','3'=>'Personal'],old('purpose'), ['class' =>'form-select','id'=>'purpose','placeholder'=>'Select Purpose'])}}
                        @if($errors->has('purpose'))
                        <div class="error">{{ $errors->first('purpose') }}</div>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <label for="visit_type" class="form-label">Visit Type</label>
                        {{Form::select('visit_type',['1'=>'Instant','2'=>'Scheduled'],old('visit_type'), ['class' => 'form-select','id'=>'visit_type','placeholder'=>'Select Visit Type'])}}
                        @if($errors->has('visit_type'))
                        <div class="error">{{ $errors->first('visit_type') }}</div>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <label for="visit_with" class="form-label">Visit With</label>
                        {{Form::select('visit_with',$employees,old('visit_with'), ['class' => 'form-select','id'=>'visit_with','placeholder'=>'Select Officer'])}}
                        @if($errors->has('visit_with'))
                        <div class="error">{{ $errors->first('visit_with') }}</div>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <label for="visit_date" class="form-label">Visit Date</label>
                        {{Form::date('visit_date',old('visit_date'), ['class' =>'form-control','id'=>'visit_date'])}}
                        @if($errors->has('visit_date'))
                        <div class="error">{{ $errors->first('visit_date') }}</div>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <label for="scheduled_time" class="form-label">Scheduled Time</label>
                        {{Form::time('scheduled_time',old('scheduled_time'), ['class' => 'form-control','id'=>'scheduled_time'])}}
                        @if($errors->has('scheduled_time'))
                        <div class="error">{{ $errors->first('scheduled_time') }}</div>
                        @endif
                    </div>
                    @if(Auth::user()->user_type == 3)
                    <div class="col-md-3">
                        <label for="entry_time" class="form-label">Entry Time</label>
                        {{Form::time('entry_time',old('entry_time'), ['class' =>$errors->first('entry_time') ? 'error-border form-control' : 'form-control','id'=>'entry_time'])}}
                        @if($errors->has('entry_time'))
                        <div class="error">{{ $errors->first('entry_time') }}</div>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <label for="exit_time" class="form-label">Exit Time</label>
                        {{Form::time('exit_time',old('exit_time'), ['class' =>$errors->first('exit_time') ? 'error-border form-control' : 'form-control','id'=>'exit_time'])}}
                        @if($errors->has('exit_time'))
                        <div class="error">{{ $errors->first('exit_time') }}</div>
                        @endif
                    </div>

                    @endif
                    <hr style="margin-top:40px">
                    <div class="col-md-3">
                        <label for="has_material" class="form-label">Has Any Material?</label>
                        {{Form::select('has_material',['1'=>'Yes','0'=>'No'],old('has_material'), ['class' =>$errors->first('has_material') ? 'error-border form-select' : 'form-select','id'=>'has_material','placeholder'=>'Select Option'])}}
                        @if($errors->has('has_material'))
                        <div class="error">{{ $errors->first('has_material') }}</div>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <label for="has_vahical" class="form-label">Has Any Vehicle?</label>
                        {{Form::select('has_vahical',['1'=>'Yes','0'=>'No'],old('has_vahical'), ['class' =>'form-select','id'=>'has_vahical','placeholder'=>'Select Option'])}}
                        @if($errors->has('has_vahical'))
                        <div class="error">{{ $errors->first('has_vahical') }}</div>
                        @endif
                    </div>
                    <div class="col-md-3 {{ isset($model) && $model->vehical_no == 1 ? '' : 'd-none'}} vehical_no_div">
                        <label for="vehical_no" class="form-label">Vehicle No</label>
                        {{Form::text('vehical_no',isset($model) ? $model->vehical_no : old('vehical_no'), ['class' => 'form-control','id'=>'vehical_no'])}}
                        @if($errors->has('vehical_no'))
                        <div class="error">{{ $errors->first('vehical_no') }}</div>
                        @endif
                    </div>

                    <hr style="margin-top:40px">
                    <h5>Material Section</h5>
                    <table class="table table-bordered">
                        <thead>
                            <tr class="{{ old('has_material') || (isset($model->has_material) && $model->has_material == 1) ? '' : 'row-disabled' }}">
                                <th>Serial No</th>
                                <th>Material Name</th>
                                <th>Description</th>
                                <th>Quantity</th>
                                <th>Is Returnable</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(old('material_name'))
                            @foreach(old('material_name') as $key => $val)
                            <tr class="{{ old('has_material') || (isset($model) && $model->has_material == 1) ? '' : 'row-disabled' }}">
                                <td>{{Form::text('serial_no['.$key.']',old('serial_no'.$key), ['class' => $errors->first('serial_no.'.$key) ? 'error-border form-control' : 'form-control','id'=>'serial_no'])}}</td>
                                <td>{{Form::text('material_name['.$key.']',old('material_name'.$key), ['class' => $errors->first('material_name.'.$key) ? 'error-border form-control' : 'form-control','id'=>'material_name'])}}</td>
                                <td>{{Form::text('description['.$key.']',old('description'.$key), ['class' => $errors->first('description.'.$key) ? 'error-border form-control' : 'form-control','id'=>'description'])}}</td>
                                <td>{{Form::number('quantity['.$key.']',old('quantity'.$key), ['class' => $errors->first('quantity.'.$key) ? 'error-border form-control' : 'form-control','id'=>'quantity'])}}</td>
                                <td> {{Form::select('is_returnable['.$key.']',['1'=>'Yes','0'=>'No'],old('is_returnable.'.$key), ['class' =>$errors->first('is_returnable.'.$key) ? 'error-border form-select' : 'form-select','placeholder'=>'Select Option'])}}</td>
                                <td>
                                    <button type="button" class="btn btn-sm btn-success add"><i class="bi bi-plus"></i></button>
                                    <button type="button" class="btn btn-sm btn-danger hide remove"><i class="bi bi-trash"></i></button>
                                </td>
                            </tr>
                            @endforeach
                            @elseif(isset($model) && $model->has_material == 1 && count($model->materials) > 0)
                            @foreach($model->materials as $key => $val)
                            <tr class="">
                                <td>{{Form::text('serial_no[]',$val->serial_no, ['class' => $errors->first('serial_no.'.$key) ? 'error-border form-control' : 'form-control','id'=>'serial_no'])}}</td>
                                <td>{{Form::text('material_name[]',$val->material_name, ['class' => $errors->first('material_name.'.$key) ? 'error-border form-control' : 'form-control','id'=>'material_name'])}}</td>
                                <td>{{Form::textarea('description[]',$val->description, ['class' => $errors->first('description.'.$key) ? 'error-border form-control' : 'form-control','rows'=>'1','id'=>'description'])}}</td>
                                <td>{{Form::number('quantity[]',$val->quantity, ['class' => $errors->first('quantity.'.$key) ? 'error-border form-control' : 'form-control','id'=>'quantity'])}}</td>
                                <td> {{Form::select('is_returnable[]',['1'=>'Yes','0'=>'No'],$val->is_returnable, ['class' =>$errors->first('is_returnable.'.$key) ? 'error-border form-select' : 'form-select','placeholder'=>'Select Option'])}}</td>
                                <td>
                                    <button type="button" class="btn btn-sm btn-success add"><i class="bi bi-plus"></i></button>
                                    <button type="button" class="btn btn-sm btn-danger hide remove"><i class="bi bi-trash"></i></button>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr class="{{ old('has_material') || (isset($model) && $model->has_material == 1) ? '' : 'row-disabled' }}">
                                <td>{{Form::text('serial_no[]',null, ['class' => 'form-control','id'=>'serial_no'])}}</td>
                                <td>{{Form::text('material_name[]',null, ['class' => 'form-control','id'=>'material_name'])}}</td>
                                <td>{{Form::textarea('description[]',null, ['class' => 'form-control','id'=>'description','rows'=>'1'])}}</td>
                                <td>{{Form::number('quantity[]',null, ['class' =>  'form-control','id'=>'quantity'])}}</td>
                                <td> {{Form::select('is_returnable[]',['1'=>'Yes','0'=>'No'],null, ['class' => 'form-select select2','placeholder'=>'Select Option'])}}</td>
                                <td>
                                    <button type="button" class="btn btn-sm btn-success add"><i class="bi bi-plus"></i></button>
                                    <button type="button" class="btn btn-sm btn-danger d-none remove"><i class="bi bi-trash"></i></button>
                                </td>
                            </tr>

                            @endif
                        </tbody>
                    </table>

                    <hr style="margin-top:40px">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success submit_button_type" clicked_val="save">Save</button>
                        @if(request()->segment(3) != 'checkin')
                        <button type="submit" class="btn btn-warning submit_button_type" clicked_val="draft">Draft</button>
                        @endif
                        <button type="reset" class="btn btn-danger">Cancel</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
</section>
@endsection

@push('custom-scripts')
<script>
    $(function() {

        $('#has_material').on('change', function() {
            var has_material = $(this).val();
            if (has_material == 1) {
                $('table > thead > tr').removeClass('row-disabled');
                $('table > tbody > tr').removeClass('row-disabled');
            } else {
                $('table > thead > tr').addClass('row-disabled');
                $('table > tbody > tr').addClass('row-disabled');
            }
        });
        $('#has_vahical').on('change', function() {
            var has_vahical = $(this).val();
            if (has_vahical == 1) {
                $('.vehical_no_div').removeClass('d-none');
            } else {
                $('.vehical_no_div').addClass('d-none');
            }
        });

        $('body').on('click', '.add', function() {
            var clone_row = $(this).parents('tr').clone();
            clone_row.find('.remove').removeClass('d-none');
            $(this).parents('tr').after(clone_row);
        });
        $('body').on('click', '.remove', function() {
            $(this).parents('tr').remove();
        });

        $('.submit_button_type').on('click', function() {
            var clickedType = $(this).attr('clicked_val');
            $('#submit_type').val(clickedType);
        });



    });

</script>
@endpush
