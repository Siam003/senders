@extends('layouts.application', [])
@section('content')
<div class="pagetitle">
    <h1>Rejected Schedule List</h1>
</div>

<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">Schedule Table</div>
                <div class="card-body">
                    <br>
                    <!-- Default Table -->
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr class="text-center">
                                <th>Schedule No</th>
                                <th>Visitor</th>
                                <th>Visit With</th>
                                <th>Purpose</th>
                                <th>Visit Date</th>
                                <th>Schedule Time</th>
                                <th>Entry </th>
                                <th>Exit</th>
                                <th>Status</th>
                                <th>Material</th>
                                <th>Vehical</th>
                            </tr>
                            @if(isset($schedules) && count($schedules) > 0)
                            @foreach($schedules as $key => $value)
                            <tr class="text-center">
                                <td>{{$value->schedule_no}}</td>
                                <td>{{$value->visitor->first_name}}</td>
                                <td>{{$value->visitEmployee->first_name}}</td>
                                <td>{{$value->purpose == 1 ? 'Official' : 'Personal'}}</td>
                                <td>{{$value->visit_date}}</td>
                                <td>{{$value->scheduled_time}}</td>
                                <td>{{$value->entry_time}}</td>
                                <td>{{$value->exit_time}}</td>
                                <td>{!! $value->status!!}</td>
                                <td>{{$value->has_material == 1 ? 'Yes' : 'No'}}</td>
                                <td>{{$value->has_vehical == 1 ? 'Yes' : 'No'}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </table>
                    </div>
                    <div class="pagination">
                        {{ $schedules->appends($_GET)->links() }}
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
@endsection
