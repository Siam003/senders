@extends('layouts.application', [])
@section('content')
<div class="pagetitle">
    <h1>Visitor All Schedules</h1>
</div>

<section class="section">
    <div style="margin-top: 10px;margin-bottom:10px">
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">Visitor Information Table</div>
                <div class="card-body">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Company</th>
                                <th>Designations</th>
                                <th>Email</th>
                                <th>Mobile No</th>
                                <th>NID</th>
                                <th>Image</th>
                                <th>Fingerprint</th>
                            </tr>
                            <tr>
                                <td>{{$model->first_name}}</td>
                                <td>{{$model->last_name}}</td>
                                <td>{{$model->visitor_company}}</td>
                                <td>{{$model->visitor_designation}}</td>
                                <td>{{$model->email}}</td>
                                <td>{{$model->mobile_no}}</td>
                                <td>{{$model->nid}}</td>
                                <td class="text-center"><img class="img-thumbnail" style="width:100px;height:100px" src="{{ asset('/uploads/user/avatar.png') }}"></td>
                                <td class="text-center"><img class="img-thumbnail" style="width:100px;height:100px" src="{{ asset('/uploads/user/finger.jpg') }}"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">Visitor Schedule Table</div>
                <div class="card-body">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th>Schedule No</th>
                                <th>Visit With</th>
                                <th>Purpose</th>
                                <th>Visit Date</th>
                                <th>Schedule Time</th>
                                <th>Entry </th>
                                <th>Exit</th>
                                <th>Status</th>
                                <th>Material</th>
                                <th>Vehical</th>
                                <th>Action</th>
                            </tr>
                            @if(isset($model->schedules) && count($model->schedules) > 0)
                            @foreach($model->schedules as $key => $value)
                            <tr>
                                <td>{{$value->schedule_no}}</td>
                                <td>{{$value->visitEmployee->first_name}}</td>
                                <td>{{$value->purpose == 1 ? 'Official' : 'Personal'}}</td>
                                <td>{{$value->visit_date}}</td>
                                <td>{{$value->scheduled_time}}</td>
                                <td>{{$value->entry_time}}</td>
                                <td>{{$value->exit_time}}</td>
                                <td>{!! $value->status!!}</td>
                                <td>{{$value->has_material == 1 ? 'Yes' : 'No'}}</td>
                                <td>{{$value->has_vehical == 1 ? 'Yes' : 'No'}}</td>
                                <td>
                                    <a href="{{url('admin/schedule/details?schedule-id='.$value->id)}}" class="btn btn-sm btn-info" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Schedule Details"><i class="bi bi-link-45deg"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
