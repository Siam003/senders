@extends('layouts.application', [])
@section('content')
<div class="pagetitle">
    <h1>Visitor Checkout</h1>
</div>

<section class="section">
    <div style="margin-top: 10px;margin-bottom:10px">
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">Returnable Material List</div>
                <div class="card-body">
                    <br>
                    @if(count($hasReturnableMaterial) > 0)
                    {!! Form::open(['url' => 'admin/schedules/checkout', 'method' => 'POST','autocomplete' => 'off', 'onsubmit' => 'submit.disabled = true;','class'=>'row g-3']) !!}
                    <input type="hidden" name="schedule_id" value="{{$model->id}}">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Material</th>
                                    <th>Serial No</th>
                                    <th>Description</th>
                                    <th>Quantity</th>
                                    <th>Returnable</th>
                                    <th>Returned</th>
                                    <th>Returned Qty</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($model->materials) && count($model->materials) > 0)
                                @foreach($model->materials->where('is_returnable', 1) as $key=>$value)
                                <tr>
                                    <input type="hidden" name="material_id[]" value="{{$value->id}}">
                                    <td>{{$value->material_name}}</td>
                                    <td>{{$value->serial_no}}</td>
                                    <td>{{$value->description}}</td>
                                    <td>{{$value->quantity}}</td>
                                    <td>{{$value->is_returnable == 1 ? 'Yes' : 'No'}}</td>
                                    <td> {{Form::select('is_returned['.$key.']',['1'=>'Yes','0'=>'No'],old('is_returned'.$key), ['class' => $errors->first('is_returned.'.$key) ? 'error-border form-control' : 'form-control','placeholder'=>'Select Option'])}}</td>
                                    <td>{{Form::number('return_quantity['.$key.']',old('return_quantity'.$key), ['class' => $errors->first('return_quantity.'.$key) ? 'error-border form-control' : 'form-control','id'=>'return_quantity'])}}</td>
                                    <td>{{Form::textarea('remarks['.$key.']',old('remarks'.$key), ['class' => $errors->first('remarks.'.$key) ? 'error-border form-control' : 'form-control','id'=>'remarks','rows'=>'1'])}}</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Save and Checkout</button>
                        <button type="reset" class="btn btn-danger">Cancel</button>
                    </div>
                    {{Form::close()}}
                    @else
                    <p>No Returnable Material</p>
                    <div class="text-center">
                        <a href="{{url('admin/schedules/clean-checkout?schedule-id='.$model->id)}}" class="btn btn-success">Clean Checkout</a>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
