@extends('layouts.application', [])
@section('content')
<div class="pagetitle">
    <h1>Visitor Schedule Details</h1>
</div>

<section class="section">
    <div style="margin-top: 10px;margin-bottom:10px">
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="float-start">
                        Visitor Information Details
                    </div>
                    <div class="float-end">
                        <a href="{{url('admin/visitor-schedule-details-download?'.Request::getQueryString())}}" class="btn btn-warning" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-original-title="Download PDF"><i class="bi bi-file-earmark-pdf"></i></a>
                    </div>
                </div>

                <div class="card-body">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="text-center">
                                    <th scope="col">First Name</th>
                                    <th scope="col">Last Name</th>
                                    <th scope="col">Company</th>
                                    <th scope="col">Designation</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Mobile No</th>
                                    <th scope="col">NID</th>
                                    <th scope="col">Photo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="text-center">
                                    <td>{{$model->visitor->first_name}}</td>
                                    <td>{{$model->visitor->last_name}}</td>
                                    <td>{{$model->visitor->visitor_company}}</td>
                                    <td>{{$model->visitor->visitor_designation}}</td>
                                    <td>{{$model->visitor->email}}</td>
                                    <td>{{$model->visitor->mobile_no}}</td>
                                    <td>{{$model->visitor->nid}}</td>
                                    <td><img class="img-thumbnail" style="width:50px;height:50px" src="{{ $model->visitor->photo ? $model->visitor->photo : asset('/uploads/user/avatar.png') }}"></td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">Visitor Schedule Table</div>
                <div class="card-body">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th>Schedule No</th>
                                <td>{{$model->schedule_no}}</td>
                                <th>Visit With</th>
                                <td>{{$model->visitEmployee->first_name}}</td>
                            </tr>
                            <tr>
                                <th>Purpose</th>
                                <td>{{$model->purpose == 1 ? 'Official' : 'Personal'}}</td>
                                <th>Visit Date</th>
                                <td>{{$model->visit_date}}</td>
                            </tr>
                            <tr>
                                <th>Schedule Time</th>
                                <td>{{$model->scheduled_time}}</td>
                                <th>Entry </th>
                                <td>{{$model->entry_time}}</td>
                            </tr>
                            <tr>
                                <th>Exit</th>
                                <td>{{$model->exit_time}}</td>
                                <th>Status</th>
                                <td>{!! $model->status !!}</td>
                            </tr>
                            <tr>
                                <th>Material</th>
                                <td>{{$model->has_material == 1 ? 'Yes' : 'No'}}</td>
                                <th>Vehical</th>
                                <td>{{$model->has_vehical == 1 ? 'Yes' : 'No'}}</td>
                            </tr>
                            <tr>
                                <th>Vehical Number</th>
                                <td>{{$model->vehical_no}}</td>
                                <th>Created By</th>
                                <td>{{$model->createdBy->name}}</td>
                            </tr>
                            <tr>
                                <th>Reviewd By</th>
                                <td>{{$model->reviewedBy->name ?? 'Pending'}}</code></td>
                                <th>Approved By</th>
                                <td>{{$model->approvedBy->name ?? 'Pending'}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">Material List</div>
                <div class="card-body">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Material</th>
                                    <th>Serial No</th>
                                    <th>Description</th>
                                    <th>Quantity</th>
                                    <th>Returnable</th>
                                    <th>Returned Qty.</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($model->materials) && count($model->materials) > 0)
                                @foreach($model->materials as $key=>$value)
                                <tr>
                                    <td>{{$value->material_name}}</td>
                                    <td>{{$value->serial_no}}</td>
                                    <td>{{$value->description}}</td>
                                    <td>{{$value->quantity}}</td>
                                    <td>{{$value->is_returnable == 1 ? 'Yes' : 'No'}}</td>
                                    <td>{{$value->returned_quantity}}</td>
                                    <td>{{$value->remarks}}</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
