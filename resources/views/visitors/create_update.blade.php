@extends('layouts.application', [])
@section('content')
<div class="pagetitle">
    <h1>Create Visitor</h1>
</div>

<section class="section">
    <div style="margin-top: 10px;margin-bottom:10px">
        <a href="{{url('admin/visitors/list')}}" class="btn btn-primary"><i class="bi bi-arrow-left-circle-fill"></i> Back</a>
    </div>
    <div class="row">
        <br>
        <div class="col-lg-9">

            <div class="card">
                <div class="card-header">
                    <h5 style="margin:0px">Visitors Information</h5>
                </div>
                <div class="card-body">
                    <br>
                    {!! Form::model($model,['url' => $model ? 'admin/visitors/'.$model->id.'/update' : 'admin/visitors/store', 'method' => $model ? 'PUT' : 'POST','autocomplete' => 'off', 'onsubmit' => 'submit.disabled = true; return true;','class'=>'row g-3','novalidate'=>'novalidate','files'=>true]) !!}
                    <input type="hidden" name="submit_type" id="submit_type">
                    <div class="col-md-4">
                        <label for="first_name" class="form-label"> First Name</label>
                        {{Form::text('first_name',null, ['class' => $errors->first('first_name') ? 'error-border form-control' : 'form-control','id'=>'first_name'])}}
                        @if($errors->has('first_name'))
                        <div class="error">{{ $errors->first('first_name') }}</div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <label for="last_name" class="form-label">Last Name</label>
                        {{Form::text('last_name',null, ['class' =>$errors->first('last_name') ? 'error-border form-control' : 'form-control','id'=>'last_name'])}}
                        @if($errors->has('last_name'))
                        <div class="error">{{ $errors->first('last_name') }}</div>
                        @endif
                    </div>
                    <div class="col-4">
                        <label for="visitor_company" class="form-label">Visitor Company</label>
                        {{Form::text('visitor_company',null, ['class' =>$errors->first('visitor_company') ? 'error-border form-control' : 'form-control','id'=>'visitor_company'])}}
                        @if($errors->has('visitor_company'))
                        <div class="error">{{ $errors->first('visitor_company') }}</div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <label for="visitor_designation" class="form-label">visitor_designation</label>
                        {{Form::email('visitor_designation',null, ['class' =>$errors->first('visitor_designation') ? 'error-border form-control' : 'form-control','id'=>'visitor_designation'])}}
                        @if($errors->has('visitor_designation'))
                        <div class="error">{{ $errors->first('visitor_designation') }}</div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <label for="nid" class="form-label">Nid</label>
                        {{Form::text('nid',null, ['class' =>$errors->first('nid') ? 'error-border form-control' : 'form-control','id'=>'nid'])}}
                        @if($errors->has('nid'))
                        <div class="error">{{ $errors->first('nid') }}</div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <label for="email" class="form-label">Email</label>
                        {{Form::email('email',null, ['class' =>$errors->first('email') ? 'error-border form-control' : 'form-control','id'=>'email'])}}
                        @if($errors->has('email'))
                        <div class="error">{{ $errors->first('email') }}</div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <label for="mobile_no" class="form-label">Mobile No</label>
                        {{Form::text('mobile_no',null, ['class' =>$errors->first('mobile_no') ? 'error-border form-control' : 'form-control','id'=>'mobile_no'])}}
                        @if($errors->has('mobile_no'))
                        <div class="error">{{ $errors->first('mobile_no') }}</div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <label for="finger_print" class="form-label">Finger Print</label>
                        {{Form::file('finger_print', ['class' =>$errors->first('finger_print') ? 'error-border form-control' : 'form-control','id'=>'finger_print'])}}
                        @if($errors->has('finger_print'))
                        <div class="error">{{ $errors->first('finger_print') }}</div>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <input type="hidden" name="photo" class="image-tag">
                    </div>
                    <hr style="margin-top:40px">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success submit_button_type" clicked_val="save_schedule">Save & Create Schedule</button>
                        <button type="submit" class="btn btn-primary submit_button_type" clicked_val="save_back">Save & Return List</button>
                        <button type="reset" class="btn btn-danger">Cancel</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">
                    <h5 style="margin:0px">Visitors Photo Fingerprint</h5>
                </div>
                <div class="card-body">
                    <br>
                    <button type="button" class="btn  btn-primary btn-block" id="takePhoto">Take Photo</button>
                    <button type="button" class="btn btn-primary" id="takePhoto" onClick="take_snapshot()">Capture</button>
                    <hr>
                    <div id="my_camera" class="text-center"></div>
                    <div id="results"></div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection

@push('custom-scripts')
<script>
    $(function() {
        $('.submit_button_type').on('click', function() {
            var clickedType = $(this).attr('clicked_val');
            $('#submit_type').val(clickedType);
        });
    });
</script>

<script language="JavaScript">
    $('#takePhoto').on('click', function() {
        $('#my_camera').removeClass('d-none');
        $('#results').addClass('d-none');
        Webcam.set({
            width: 275,
            height: 195,
            image_format: 'jpeg',
            jpeg_quality: 90,
        });

        // Attach camera here
        Webcam.attach('#my_camera');
    });

    function take_snapshot() {
        Webcam.snap(function(data_uri) {
            $('#my_camera').addClass('d-none');
            $('#results').removeClass('d-none');
            $(".image-tag").val(data_uri);
            document.getElementById('results').innerHTML = '<img src="' + data_uri + '"/>';
        });
        Webcam.reset();
    }
</script>
@endpush