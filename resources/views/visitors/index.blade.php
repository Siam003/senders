@extends('layouts.application', [])
@section('content')
<div class="pagetitle">
    <h1>Visitors List</h1>
</div>

<section class="section">
    <div class="float-start">
        <a href="{{url('admin/visitors/create')}}" class="btn btn-success"><i class="bi bi-plus"></i> Create Visitor</a>
    </div>
    <div class="float-end">
        <form action="{{url('admin/visitors/list')}}" method="get" class="row row-cols-lg-auto g-3 align-items-center">
            <div class="col-12">
                <label class="visually-hidden" for="inlineFormSelectPref">Preference</label>
                {{Form::select('search_params',$search_params,$search_array ? $search_array['search_params'] : '', ['class' =>'form-select','placeholder'=>'Choose'])}}
            </div>
            <div class="col-12">
                <label class="visually-hidden" for="inlineFormInputGroupUsername">Username</label>
                <div class="input-group">
                    {{Form::text('keyword',$search_array ? $search_array['keyword'] : '', ['class' =>  'form-control','placeholder'=>'Keyword'])}}
                </div>
            </div>
            <div class="col-12">
                <button type="submit" class="btn btn-primary"><i class="bi bi-search"></i> Search</button>
                <a href="{{url('admin/visitors')}}" class="btn btn-warning"><i class="bi bi-trash"></i> Reset</a>
            </div>
        </form>
    </div>
    <div class="clearfix"></div>
    <div class="row" style="margin-top: 10px;margin-bottom:10px">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">Visitors Table</div>
                <div class="card-body">
                    <br>
                    <!-- Default Table -->
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="text-center">
                                    <th scope="col">First Name</th>
                                    <th scope="col">Last Name</th>
                                    <th scope="col">Company</th>
                                    <th scope="col">Designation</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Mobile No</th>
                                    <th scope="col">NID</th>
                                    <th scope="col">Photo</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($visitors as $visitor)
                                <tr class="text-center">
                                    <td>{{$visitor->first_name}}</td>
                                    <td>{{$visitor->last_name}}</td>
                                    <td>{{$visitor->visitor_company}}</td>
                                    <td>{{$visitor->visitor_designation}}</td>
                                    <td>{{$visitor->email}}</td>
                                    <td>{{$visitor->mobile_no}}</td>
                                    <td>{{$visitor->nid}}</td>
                                    <td><img class="img-thumbnail" style="width:50px;height:50px" src="{{ $visitor->photo ? $visitor->photo : asset('/uploads/user/avatar.png') }}"></td>
                                    <td>
                                        <ul class="d-flex align-items-center" style="list-style: none;">
                                            <li class="nav-item dropdown pe-3">
                                                <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
                                                    <span class="d-none d-md-block dropdown-toggle ps-2">Action</span>
                                                </a><!-- End Profile Iamge Icon -->
                                                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                                                    <li>
                                                        <a class="dropdown-item d-flex align-items-center" href="{{url('admin/visitors/edit/'.$visitor->id)}}">
                                                            <i class="bi bi-pencil-square"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="dropdown-item d-flex align-items-center" href="{{url('admin/visitors/delete/'.$visitor->id)}}" onclick="return confirm('Are you sure?')">
                                                            <i class="bi bi-trash"></i>
                                                            <span>Delete</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="dropdown-item d-flex align-items-center" href="{{url('admin/schedule/create?visitor-id='.$visitor->id)}}">
                                                            <i class="bi bi-card-checklist"></i>
                                                            <span>Create Schedule</span>
                                                        </a>
                                                    </li>

                                                </ul>
                                            </li>

                                        </ul>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pagination">
                        {{ $visitors->appends($_GET)->links() }}
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
@endsection
@push('custom-scripts')
<script>
    $(function() {

    });

</script>
@endpush
