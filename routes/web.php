<?php

use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('login');
})->name('login');

Route::post('system-authenticattion/login', 'SystemAuthenticattionController@login');


Route::group(['middleware' => 'auth', 'prefix' => '/admin',], function () {
    Route::get('dashboard', 'AdminController@index');
    Route::get('logout', 'AdminController@logout');
    Route::get('users', 'UserController@index');
    Route::get('users/create', 'UserController@create');
    Route::post('users/store', 'UserController@store');
    Route::get('users/edit/{id}', 'UserController@edit');
    Route::put('users/{id}/update', 'UserController@store');
    Route::get('users/delete/{id}', 'UserController@delete');

    // visitors
    Route::get('visitors/list', 'VisitorsController@index');
    Route::get('visitors/create', 'VisitorsController@create');
    Route::post('visitors/store', 'VisitorsController@store');
    Route::get('/visitors/edit/{id}', 'VisitorsController@edit');
    Route::put('visitors/{id}/update', 'VisitorsController@store');

    Route::get('schedules/create', 'ScheduleController@createSchedule');
    Route::post('schedule/store', 'ScheduleController@storeSchedule');
    Route::get('schedules/edit/{id}', 'ScheduleController@edit');
    Route::get('schedules/checkin', 'ScheduleController@edit');
    Route::put('schedule/{id}/update', 'ScheduleController@storeSchedule');
    Route::get('schedules/checkout', 'AdminController@checkout');
    Route::post('schedules/checkout', 'AdminController@checkoutProcess');
    Route::get('schedules/clean-checkout', 'AdminController@cleanCheckout');

    Route::get('schedules', 'ScheduleController@schedules');
    Route::get('schedule/details', 'ScheduleController@scheduleDetails');
    Route::get('schedules/list', 'ScheduleController@index');

    Route::get('schedules/pending-review', 'ScheduleController@pendingReview');
    Route::get('schedules/schedule-review-details', 'ScheduleController@scheduleReviewDetails');
    Route::get('schedules/reviewd', 'ScheduleController@scheduleReviewd');
    Route::get('schedule/reject', 'ScheduleController@scheduleReject');
    Route::get('schedules/rejected-list', 'ScheduleController@rejectedList');
    
    Route::get('schedules/pending-approval', 'ScheduleController@pendingApproval');
    Route::get('schedules/approved', 'ScheduleController@approved');
    Route::get('schedules/approved-list', 'ScheduleController@approvedList');
    Route::get('schedules/gatepass-details', 'ScheduleController@gatepassDetails');
    Route::get('visitor-gatepass-download', 'ScheduleController@gatepassDownload');
    

    Route::get('schedule/occurance-entry','OccuranceController@occuranceEntry');
    Route::post('schedule/occurance-entry','OccuranceController@occuranceStore');


    Route::get('visitor-history', 'VisitorsController@visitorHistory');
    Route::get('visitor-history-list-download', 'VisitorsController@visitorHistoryList');
    Route::get('visitor-schedule-details-download', 'ScheduleController@visitorScheduleDetailsDownload');


});
// admin routes