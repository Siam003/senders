<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Visitor;
use Faker\Generator as Faker;


$factory->define(Visitor::class, function (Faker $faker) {
    return [
        'first_name'                    => $faker->name,
        'last_name'                 => $faker->name,
        'visitor_company'           => $faker->company,
        'visitor_designation'       => $faker->text(5),
        'mobile_no'                 => $faker->randomNumber(5),
        'nid'                       => $faker->randomNumber(5),
        'email'                     => $faker->unique()->safeEmail,
    ];
});
