<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\VisitorSchedule;
use Faker\Generator as Faker;


$factory->define(VisitorSchedule::class, function (Faker $faker) {
    return [
        'visitor_id'                    => $faker->numberBetween(1,15),
        'schedule_no'                   => $faker->randomNumber(5),
        'purpose'                       => $faker->numberBetween(1,3),
        'has_material'                  => 1,
        'has_vahical'                   => 1,
        'vehical_no'                    => $faker->randomNumber(5),
        'visit_type'                    => $faker->numberBetween(1,2),
        'status'                        => $faker->numberBetween(1,2),
        'visit_date'                    => $faker->dateTimeThisMonth(),
        'scheduled_time'                => $faker->time(),
        'created_by'                    => $faker->numberBetween(1,20),
        'visit_with'                    => $faker->numberBetween(1,3),
    ];
});
