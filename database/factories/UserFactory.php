<?php



/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;


$factory->define(User::class, function (Faker $faker) {
    return [
        'name'              => $faker->name,
        'official_id'        => $faker->randomNumber(5),
        'designation_id'    => $faker->numberBetween(1,5),
        'department_id'     => $faker->numberBetween(1,6),
        'user_name'         => $faker->userName,
        'user_type'         => $faker->numberBetween(1,3),
        'email'             => $faker->unique()->safeEmail,
        'password'          => bcrypt('123456'), // password
    ];;
});
