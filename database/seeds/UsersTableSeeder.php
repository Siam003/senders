<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use JeroenZwart\CsvSeeder\CsvSeeder;

class UsersTableSeeder extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('users')->insert([
        //     'name' => 'Super Admin',
        //     'user_name' => 'super_admin',
        //     'email' => 'super_admin@gmail.com',
        //     'password' => bcrypt('123456'),
        //     'user_type'=>0
        // ]);

        factory(App\User::class, 10)->create();
    }
}
