<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitorSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitor_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('visitor_id');
            $table->string('schedule_no');
            $table->tinyInteger('purpose')->comment('1 = official, 2 = personal');
            $table->tinyInteger('has_material');
            $table->tinyInteger('has_vahical');
            $table->string('vehical_no')->nullable();
            $table->string('visit_type')->comment('1 = instant, 2 = scheduled');
            $table->string('visit_with');
            $table->string('status')->comment('1 = draft, 2 = created, 3 = reviewed and send for approval, 4 = approved, 5 = rejected ');
            $table->date('visit_date');
            $table->dateTime('scheduled_time');
            $table->string('entry_time')->nullable();
            $table->string('exit_time')->nullable();
            $table->string('created_by');
            $table->string('approved_by')->nullable();
            $table->string('reviewed_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitor_schedules');
    }
}
