<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitorsMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitors_materials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('visitor_id');
            $table->integer('schedule_id');
            $table->string('material_name');
            $table->string('serial_no')->nullable();
            $table->text('description')->nullable();
            $table->string('quantity')->default(0);
            $table->tinyInteger('is_returnable')->default(0);
            $table->tinyInteger('is_returned')->nullable();
            $table->integer('return_quantity')->nullable();
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitors_materials');
    }
}
