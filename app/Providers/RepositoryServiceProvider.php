<?php

namespace App\Providers;

use App\Repositories\UserRepository;
use App\Repositories\VisitorRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\EmployeeRepository;
use App\Repositories\VisitorsMaterialRepository;
use App\Repositories\VisitorsScheduleRepository;
use App\Repositories\VisitorsOccurrenceRepository;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Interfaces\VisitorRepositoryInterface;
use App\Repositories\Interfaces\EmployeeRepositoryInterface;
use App\Repositories\Interfaces\VisitorsMaterialRepositoryInterface;
use App\Repositories\Interfaces\VisitorsScheduleRepositoryInterface;
use App\Repositories\Interfaces\VisitorsOccurrenceRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(VisitorRepositoryInterface::class, VisitorRepository::class);
        $this->app->bind(VisitorsScheduleRepositoryInterface::class, VisitorsScheduleRepository::class);
        $this->app->bind(VisitorsMaterialRepositoryInterface::class, VisitorsMaterialRepository::class);
        $this->app->bind(EmployeeRepositoryInterface::class, EmployeeRepository::class);
        $this->app->bind(VisitorsOccurrenceRepositoryInterface::class, VisitorsOccurrenceRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
