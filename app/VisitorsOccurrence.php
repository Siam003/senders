<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitorsOccurrence extends Model
{
    protected $table = 'visitors_occurrences';
    protected $fillable = [
        'schedule_id', 'occurance_type', 'occurance_details', 'created_at', 'updated_at'
    ];

    public function schedule(){
        return $this->belongsTo(VisitorSchedule::class, 'schedule_id' ,'id')->withDefault();
    }
}
