<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserLoginRequest;
use Illuminate\Support\Facades\Redirect;
use App\Helpers\Interfaces\PaginationHelperInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;

class SystemAuthenticattionController extends Controller
{
    /** @var \App\Repositories\Interfaces\UserRepositoryInterface $userRepository */
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository   =   $userRepository;
    }

    /**
     * @param Request $request
     * 
     * @return view
     */
    public function login(UserLoginRequest $request)
    {

        $userName       =   $request->get('username');
        $password       =   $request->get('password');

        if (Auth::attempt(['user_name' => $userName, 'password' => $password])) {
            return Redirect::to('admin/dashboard');
        }
        return Redirect::back()->withErrors(['msg' => 'Something went wrong. Please try again!']);
    }
}
