<?php

namespace App\Http\Controllers;

use Exception;
use App\VisitorSchedule;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ScheduleRequest;
use App\Repositories\Interfaces\VisitorRepositoryInterface;
use App\Repositories\Interfaces\EmployeeRepositoryInterface;
use App\Repositories\Interfaces\VisitorsMaterialRepositoryInterface;
use App\Repositories\Interfaces\VisitorsScheduleRepositoryInterface;

class ScheduleController extends Controller
{


    /** @var \App\Repositories\Interfaces\VisitorRepositoryInterface $visitorRepository */
    protected $visitorRepository;

    /** @var \App\Repositories\Interfaces\VisitorsScheduleRepositoryInterface $visitorsScheduleRepository */
    protected $visitorsScheduleRepository;

    /** @var \App\Repositories\Interfaces\VisitorsMaterialRepositoryInterface $visitorsMaterialRepository */
    protected $visitorsMaterialRepository;

    /** @var \App\Repositories\Interfaces\EmployeeRepositoryInterface $employeeRepository */
    protected $employeeRepository;


    public function __construct(
        VisitorRepositoryInterface $visitorRepository,
        VisitorsScheduleRepositoryInterface $visitorsScheduleRepository,
        VisitorsMaterialRepositoryInterface $visitorsMaterialRepository,
        EmployeeRepositoryInterface $employeeRepository
    ) {
        $this->visitorRepository            =   $visitorRepository;
        $this->visitorsScheduleRepository   =   $visitorsScheduleRepository;
        $this->visitorsMaterialRepository   =   $visitorsMaterialRepository;
        $this->employeeRepository           =   $employeeRepository;
    }

    public function index(Request $request)
    {
        $searchArray['date']        =   $request->date;
        $searchArray['status']      =   $request->status;
        $searchArray['keyword']     =   $request->keyword;
        $searchArray['visitor_id']  =   $request->visitor_id;
        $searchArray['visit_with']  =   $request->visit_with;
        $searchArray['schedule_no']  =   $request->schedule_no;

        return view('schedule/index', [
            'schedules'     => $this->visitorsScheduleRepository->getAllSchedules($searchArray),
            'visitors'      =>  $this->visitorRepository->getModelInstance()->pluck('first_name', 'id'),
            'employees'     =>  $this->employeeRepository->getModelInstance()->pluck('first_name', 'id'),
            'search_array'  => $searchArray,
            'status'        =>  [
                1 => 'Draft',
                2 => 'Created',
                3 => 'Reviewed',
                4 => 'Approved',
                5 => 'Rejected',
            ],
        ]);
    }

    public function createSchedule(Request $request)
    {
        return view('schedule/create_update', [
            'schedule_no'   => random_int(100, 100000),
            'model'         =>  null,
            'employees'     =>  $this->employeeRepository->getModelInstance()->pluck('first_name', 'id'),
            'visitors'      =>  $this->visitorRepository->getModelInstance()->pluck('email', 'id'),
            'visitor_id'    =>  $request->get('visitor-id')
        ]);
    }

    public function storeSchedule(ScheduleRequest $request)
    {

        try {
            DB::beginTransaction();
            $schedule = $this->visitorsScheduleRepository->createUpdate($request);
            if ($request->has_material) {
                if ($request->material_name) {
                    $this->visitorsMaterialRepository->getModelInstance()->where(['schedule_id' => $request->schedule_id])->delete();
                }
                $this->visitorsMaterialRepository->createBulk($request, $schedule);
            }
            DB::commit();
            return redirect()->to('admin/schedules/list')->with('success', 'Success!! Data Stored Successfully.');
        } catch (Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('error', $exception->getMessage());
        }
    }

    public function edit($id = null, Request $request)
    {
        $id = $id ?? $request->get('schedule-id');
        $model = $this->visitorsScheduleRepository->getModelInstance()->with('materials')->find($id);

        return view('schedule/create_update', [
            'schedule_no'   => random_int(100, 100000),
            'model'         =>  $model,
            'employees'     =>  $this->employeeRepository->getModelInstance()->pluck('first_name', 'id'),
            'visitors'      =>  $this->visitorRepository->getModelInstance()->pluck('email', 'id'),
            'visitor_id'    =>  $model->visitor_id,
        ]);
    }

    public function schedules(Request $request)
    {
        $visitorId = $request->get('visitor-id');
        $model = $this->visitorRepository->getModelInstance()->with('schedules', 'schedules.materials', 'schedules.visitEmployee', 'schedules.createdBy', 'schedules.approvedBy', 'schedules.reviewedBy')->find($visitorId);
        return view('schedule/visitors_schedule', [
            'model'         =>  $model,
            'visitor_id'    =>  $request->get('visitor-id')
        ]);
    }

    public function scheduleDetails(Request $request)
    {
        $scheduleId = $request->get('schedule-id');
        $model = $this->visitorsScheduleRepository->getModelInstance()->with('materials', 'visitEmployee', 'createdBy', 'approvedBy', 'reviewedBy')->find($scheduleId);
        return view('schedule/details', [
            'model'         =>  $model
        ]);
    }

    public function pendingReview(Request $request)
    {
        // search params
        $searchArray['date']        =   $request->date;
        $searchArray['status']      =   $request->status;
        $searchArray['keyword']     =   $request->keyword;
        $searchArray['visitor_id']  =   $request->visitor_id;
        $searchArray['visit_with']  =   $request->visit_with;
        $searchArray['schedule_no'] =   $request->schedule_no;

        $userDepartment         =   Auth::user()->department_id;
        $allSameDeptEmployees   =   $this->employeeRepository->getModelInstance()->where('department_id', $userDepartment)->pluck('id');
        $query                  =   $this->visitorsScheduleRepository->generateSearchQuery($searchArray)->where('status', VisitorSchedule::CREATED_STATUS);
        
        if (Auth::user()->user_type != 0) {
            $query = $query->whereIn('visit_with', $allSameDeptEmployees);
        }

        $allPendingReviewSchedule = $query->paginate();

        return view('schedule/pending_review_schedule', [
            'schedules'         =>  $allPendingReviewSchedule,
            'visitors'          =>  $this->visitorRepository->getModelInstance()->pluck('first_name', 'id'),
            'employees'         =>  $this->employeeRepository->getModelInstance()->pluck('first_name', 'id'),
            'search_array'      =>  $searchArray,
            'status'            =>  
            [
                1 => 'Draft',
                2 => 'Created',
                3 => 'Reviewed',
                4 => 'Approved',
                5 => 'Rejected',
            ],

        ]);
    }

    public function scheduleReviewDetails(Request $request)
    {
        $scheduleId = $request->get('schedule-id');
        $model = $this->visitorsScheduleRepository->getModelInstance()->with('visitor', 'materials', 'visitEmployee', 'createdBy', 'approvedBy', 'reviewedBy')->find($scheduleId);
        return view('schedule/schedule_review_details', [
            'model'         =>  $model
        ]);
    }

    public function scheduleReviewd(Request $request)
    {
        try {
            DB::beginTransaction();
            $scheduleId = $request->get('schedule-id');
            $model = $this->visitorsScheduleRepository->makeReviewd($scheduleId);
            DB::commit();
            if ($model) {
                return redirect()->to('admin/schedules/pending-review')->with('success', 'Success!! Schedule reviewd send for approval.');
            }
        } catch (Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('error', $exception->getMessage());
        }
    }

    public function scheduleReject(Request $request)
    {
        try {
            DB::beginTransaction();
            $scheduleId = $request->get('schedule-id');
            $model = $this->visitorsScheduleRepository->makeReject($scheduleId);
            DB::commit();
            if ($model) {
                return redirect()->to('admin/schedules/pending-review')->with('error', 'Failed!! Schedule Rejected.');
            }
        } catch (Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('error', $exception->getMessage());
        }
    }

    public function pendingApproval(Request $request)
    {

        // search params
        $searchArray['date']        =   $request->date;
        $searchArray['status']      =   $request->status;
        $searchArray['keyword']     =   $request->keyword;
        $searchArray['visitor_id']  =   $request->visitor_id;
        $searchArray['visit_with']  =   $request->visit_with;
        $searchArray['schedule_no'] =   $request->schedule_no;
        
        $userDepartment         =   Auth::user()->department_id;
        $allSameDeptEmployees   =   $this->employeeRepository->getModelInstance()->where('department_id', $userDepartment)->pluck('id');
        $query                  =   $this->visitorsScheduleRepository->generateSearchQuery($searchArray)->where('status', VisitorSchedule::REVIEWD_STATUS);

        if (Auth::user()->user_type != 0) {
            $query = $query->whereIn('visit_with', $allSameDeptEmployees);
        }

        $allPendingApprovalSchedule = $query->paginate();

        return view('schedule/pending_approval_schedule', [
            'schedules'         =>  $allPendingApprovalSchedule,
            'visitors'          =>  $this->visitorRepository->getModelInstance()->pluck('first_name', 'id'),
            'employees'         =>  $this->employeeRepository->getModelInstance()->pluck('first_name', 'id'),
            'search_array'      =>  $searchArray,
            'status'            =>  
            [
                1 => 'Draft',
                2 => 'Created',
                3 => 'Reviewed',
                4 => 'Approved',
                5 => 'Rejected',
            ],
        ]);
    }

    public function approved(Request $request)
    {
        try {
            DB::beginTransaction();
            $scheduleId = $request->get('schedule-id');
            $model = $this->visitorsScheduleRepository->makeApproved($scheduleId);
            DB::commit();
            if ($model) {
                return redirect()->to('admin/schedules/pending-approval')->with('success', 'Success!! Schedule Approved.');
            }
        } catch (Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('error', $exception->getMessage());
        }
    }

    public function approvedList(Request $request)
    {
        // search params
        $searchArray['date']        =   $request->date;
        $searchArray['status']      =   $request->status;
        $searchArray['keyword']     =   $request->keyword;
        $searchArray['visitor_id']  =   $request->visitor_id;
        $searchArray['visit_with']  =   $request->visit_with;
        $searchArray['schedule_no'] =   $request->schedule_no;

        $allApprovedSchedule =  $this->visitorsScheduleRepository->generateSearchQuery($searchArray)->where('status', VisitorSchedule::APPROVED_STATUS)->paginate();
        
        return view('schedule/approved_schedule', [
            'schedules'         =>  $allApprovedSchedule,
            'visitors'          =>  $this->visitorRepository->getModelInstance()->pluck('first_name', 'id'),
            'employees'         =>  $this->employeeRepository->getModelInstance()->pluck('first_name', 'id'),
            'search_array'      =>  $searchArray,
            'status'            =>  
            [
                1 => 'Draft',
                2 => 'Created',
                3 => 'Reviewed',
                4 => 'Approved',
                5 => 'Rejected',
            ],

        ]);
    }

    public function gatepassDetails(Request $request)
    {
        $scheduleId = $request->get('schedule-id');
        $model = $this->visitorsScheduleRepository->getModelInstance()->with('visitor', 'materials', 'visitEmployee', 'createdBy', 'approvedBy', 'reviewedBy')->find($scheduleId);
        return view('schedule/gatepass_details', [
            'model'         =>  $model
        ]);
    }

    public function rejectedList()
    {
        $userDepartment = Auth::user()->department_id;
        $allSameDeptEmployees = $this->employeeRepository->getModelInstance()->where('department_id', $userDepartment)->pluck('id');

        $query =  $this->visitorsScheduleRepository->getModelInstance()->where('status', VisitorSchedule::REJECTED_STATUS);
        if (Auth::user()->user_type != 0) {
            $query = $query->whereIn('visit_with', $allSameDeptEmployees);
        }
        $allRejectedSchedule = $query->paginate();
        return view('schedule/rejected_schedule', [
            'schedules'         =>  $allRejectedSchedule
        ]);
    }

    public function visitorScheduleDetailsDownload(Request $request)
    {
        $scheduleId     =   $request->get('schedule-id');
        $model          =   $this->visitorsScheduleRepository->getModelInstance()->with('materials', 'visitEmployee', 'createdBy', 'approvedBy', 'reviewedBy')->find($scheduleId);
        $pdf            =   Pdf::loadView('reports/visitor-schedule-details-download-pdf', ['model' => $model])->setPaper('a4', 'landscape');
        return $pdf->download('visitor-schedule-details.pdf');
    }

    public function gatepassDownload(Request $request){
        $scheduleId     =   $request->get('schedule-id');
        $model          =   $this->visitorsScheduleRepository->getModelInstance()->with('visitor', 'materials', 'visitEmployee', 'createdBy', 'approvedBy', 'reviewedBy')->find($scheduleId);
        $pdf            =   Pdf::loadView('reports/gatepass', ['model' => $model])->setPaper('a4', 'landscape');
        return $pdf->download('visitor-gatepass.pdf');
    }
}
