<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ScheduleRequest;
use App\Http\Requests\VisitorCreateRequest;
use App\Repositories\Interfaces\VisitorRepositoryInterface;
use App\Repositories\Interfaces\VisitorsMaterialRepositoryInterface;
use App\Repositories\Interfaces\VisitorsScheduleRepositoryInterface;
use App\User;
use App\VisitorSchedule;
use Barryvdh\DomPDF\Facade\Pdf as pdf;

class VisitorsController extends Controller
{


    /** @var \App\Repositories\Interfaces\VisitorRepositoryInterface $visitorRepository */
    protected $visitorRepository;

    /** @var \App\Repositories\Interfaces\VisitorsScheduleRepositoryInterface $visitorsScheduleRepository */
    protected $visitorsScheduleRepository;

    /** @var \App\Repositories\Interfaces\VisitorsMaterialRepositoryInterface $visitorsMaterialRepository */
    protected $visitorsMaterialRepository;


    public function __construct(
        VisitorRepositoryInterface $visitorRepository,
        VisitorsScheduleRepositoryInterface $visitorsScheduleRepository,
        VisitorsMaterialRepositoryInterface $visitorsMaterialRepository
    ) {
        $this->visitorRepository            =   $visitorRepository;
        $this->visitorsScheduleRepository   =   $visitorsScheduleRepository;
        $this->visitorsMaterialRepository   =   $visitorsMaterialRepository;
    }

    public function index(Request $request)
    {
        $searchArray = [];
        if ($request->search_params && $request->keyword) {
            $searchArray['search_params']   =   $request->search_params;
            $searchArray['keyword']         =   $request->keyword;
        }

        $visitors = $this->visitorRepository->getAllVisitors($searchArray);
        return view('visitors/index', [
            'visitors' => $visitors,
            'search_array' => $searchArray,
            'search_params' => ['visitor_company' => 'Company', 'visitor_designation' => 'Designarion', 'mobile_no' => 'Mobile No', 'email' => 'Email', 'nid' => 'NID',]
        ]);
    }

    public function create()
    {
        return view('visitors/create_update', [
            'model'         =>  null,
        ]);
    }

    public function store(VisitorCreateRequest $request)
    {
        try {
            DB::beginTransaction();
            $data  = $this->visitorRepository->createUpdate($request);
            DB::commit();

            if ($request->submit_type == 'save_schedule') {
                return redirect()->to('admin/schedule/create?visitor-id=' . $data->id)->with('success', 'Data Stored Successfully');
            }
            return redirect()->to('admin/visitors/list')->with('success', 'Data Stored Successfully');
        } catch (Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage())->withInput();
        }
    }

    public function edit($id, Request $request)
    {
        return view('visitors/create_update', [
            'model'         =>  $this->visitorRepository->getModelInstance()->find($id),
        ]);
    }

    public function visitorHistory(Request $request)
    {
        $searchArray = [];
        if ($request->query->count() > 0) {
            $searchArray['name']                =   $request->name;
            $searchArray['status']              =   $request->status;
            $searchArray['to_date']             =   $request->to_date;
            $searchArray['from_date']           =   $request->from_date;
            $searchArray['mobile_no']           =   $request->mobile_no;
            $searchArray['department_id']       =   $request->department_id;
            $searchArray['visitor_company']     =   $request->visitor_company;
        }

        $visitorsDepartments    =   User::DEPARTMENTS;
        $status                 =   VisitorSchedule::STATUS;
        $schedules              =   $this->visitorsScheduleRepository->getAllScheduleData($searchArray);
        $visitorsCompany        =   $this->visitorRepository->getModelInstance()->pluck('visitor_company', 'id');

        return view('reports/visitor_history_list', [
            'visitorsCompany'           =>  $visitorsCompany,
            'visitorsDepartment'        =>  $visitorsDepartments,
            'search_array'              =>  $searchArray,
            'schedules'                 =>  $schedules,
            'status'                    =>  $status,
        ]);
    }

    public function visitorHistoryList(Request $request)
    {
        $searchArray = [];
        if ($request->query->count() > 0) {
            $searchArray['name']                =   $request->name;
            $searchArray['status']              =   $request->status;
            $searchArray['to_date']             =   $request->to_date;
            $searchArray['from_date']           =   $request->from_date;
            $searchArray['mobile_no']           =   $request->mobile_no;
            $searchArray['department_id']       =   $request->department_id;
            $searchArray['visitor_company']     =   $request->visitor_company;
        }

        $schedules              =   $this->visitorsScheduleRepository->getAllScheduleData($searchArray);
        
        $pdf = Pdf::loadView('reports/visitor_history_list_pdf', ['schedules' => $schedules, 'search_array' =>  $searchArray,])->setPaper('a4', 'landscape');
        return $pdf->download('visitor-history.pdf');
    }
}
