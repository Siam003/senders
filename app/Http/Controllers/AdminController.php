<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CheckOutRequest;
use App\Repositories\Interfaces\VisitorsMaterialRepositoryInterface;
use App\Repositories\Interfaces\VisitorsScheduleRepositoryInterface;

class AdminController extends Controller
{

    /** @var \App\Repositories\Interfaces\VisitorsScheduleRepositoryInterface $visitorsScheduleRepository */
    protected $visitorsScheduleRepository;

    /** @var \App\Repositories\Interfaces\VisitorsMaterialRepositoryInterface $visitorsMaterialRepository */
    protected $visitorsMaterialRepository;


    public function __construct(
        VisitorsScheduleRepositoryInterface $visitorsScheduleRepository,
        VisitorsMaterialRepositoryInterface $visitorsMaterialRepository
    ) {
        $this->visitorsScheduleRepository   =   $visitorsScheduleRepository;
        $this->visitorsMaterialRepository   =   $visitorsMaterialRepository;
    }

    public function index()
    {
        $today = \Carbon\Carbon::now()->toDateString();
        $todaysSchedule = $this->visitorsScheduleRepository->getDataByDate($today);
        $todaysTotalIn = $this->visitorsScheduleRepository->getTotalByDateStatus($today, 'in');
        $todaysTotalOut = $this->visitorsScheduleRepository->getTotalByDateStatus($today, 'out');

        return view('admin.index', [
            'todaysSchedule' => $todaysSchedule->load('visitor'),
            'todaysTotalIn' => $todaysTotalIn,
            'total_schedule' => count($todaysSchedule),
            'todays_total_in' => count($todaysTotalIn),
            'todays_total_out' => count($todaysTotalOut),
        ]);
    }

    public function checkout(Request $request)
    {
        $scheduleId = $request->get('schedule-id');
        $hasReturnableMaterial = $this->visitorsMaterialRepository->getModelInstance()->where([
            'schedule_id' => $scheduleId,
            'is_returnable' => 1,
        ])->get();
        $model = $this->visitorsScheduleRepository->getModelInstance()->with('materials')->find($scheduleId);
        return view('schedule/checkout', [
            'model' => $model,
            'hasReturnableMaterial' => $hasReturnableMaterial
        ]);
    }

    public function checkoutProcess(CheckOutRequest $request)
    {

        try {
            DB::beginTransaction();
            $this->visitorsScheduleRepository->getModelInstance()->where('id', $request->schedule_id)
                ->update([
                    'exit_time' => date('h:i a'),
                ]);
            $this->visitorsMaterialRepository->saveReturnMaterial($request->all());
            DB::commit();
            return redirect()->to('admin/dashboard')->with('success', 'Success!! Visitor checkout done successfully!!');
        } catch (Exception $exception) {
            DB::rollBack();
        }
    }

    public function cleanCheckout(Request $request)
    {
        try {
            DB::beginTransaction();
            $this->visitorsScheduleRepository->getModelInstance()->where('id', $request->get('schedule-id'))
                ->update([
                    'exit_time' => date('h:i a'),
                ]);
            DB::commit();
            return redirect()->to('admin/dashboard')->with('success', 'Success!! Visitor checkout done successfully!!');
        } catch (Exception $exception) {
            DB::rollBack();
        }
    }


    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
