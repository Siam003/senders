<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\OccuranceRequest;
use App\Repositories\Interfaces\VisitorsOccurrenceRepositoryInterface;

class OccuranceController extends Controller
{

    /** @var \App\Repositories\Interfaces\VisitorsOccurrenceRepositoryInterface $visitorOccurrence */
    protected $visitorOccurrence;


    public function __construct(
        VisitorsOccurrenceRepositoryInterface $visitorOccurrence
    ) {
        $this->visitorOccurrence            =   $visitorOccurrence;
    }


    public function occuranceEntry(Request $request)
    {
        return view('occurance/create', [
            'schedule_id'         =>  $request->get('schedule-id'),
        ]);
    }

    public function occuranceStore(OccuranceRequest $request)
    {
        try {
            DB::beginTransaction();
            $this->visitorOccurrence->createUpdate($request->all());
            DB::commit();
            return redirect()->back()->withInput()->with('success', 'Success!! Data Stored Successfully.');
        } catch (Exception $exception) {
            dd($exception->getMessage());
            DB::rollBack();
            redirect()->back()->withInput()->with('error', $exception->getMessage());
        }
    }
}
