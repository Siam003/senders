<?php

namespace App\Http\Controllers;

use App\User;
use Exception;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\DB;
use App\Repositories\Interfaces\UserRepositoryInterface;

class UserController extends Controller
{
    /** @var \App\Repositories\Interfaces\UserRepositoryInterface $userRepository */
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository   =   $userRepository;
    }

    public function index()
    {
        $users =  $this->userRepository->getAllUser([]);
        return view('users/index', [
            'users' => $users
        ]);
    }

    public function create()
    {
        return view('users/create_update', [
            'model'         =>  null,
            'user_types'    =>  User::USER_TYPE,
            'departments'   =>  User::DEPARTMENTS,
            'designations'  =>  User::DESIGNATION,
        ]);
    }

    public function store(UserRequest $request)
    {
        try {
            DB::beginTransaction();
            $this->userRepository->createUpdate($request);
            DB::commit();
            return redirect()->back()->with('success', 'Data Stored Successfully');
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'Data Stored Failed!');
        }
    }

    public function edit(Request $request, $id)
    {
        return view('users/create_update', [
            'model'         =>  $this->userRepository->getModelInstance()->find($id),
            'user_types'    =>  User::USER_TYPE,
            'departments'   =>  User::DEPARTMENTS,
            'designations'  =>  User::DESIGNATION,
        ]);
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $this->userRepository->deleteUser($id);
            DB::commit();
            return redirect()->back()->with('success', 'Data Deleted Successfully');
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'Data Deleted Failed!');
        }
    }
}
