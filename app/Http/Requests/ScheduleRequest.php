<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'purpose'           =>  'required',
            'visit_type'        =>  'required',
            'visit_with'        =>  'required',
            'visit_date'        =>  'required',
            'scheduled_time'    =>  'required',
            'has_material'      =>  'required',
            'has_vahical'       =>  'required',
            'visitor_id'        =>  'required',
        ];
        if(request()->get('check_in')){
            $rules['entry_time']   =   'required';
        }

        if ($this->has_material == 1) {
            $rules['material_name.*']   =   'required';
            $rules['quantity.*']        =   'required';
            $rules['is_returnable.*']   =   'required';
        }
        return  $rules;
    }
}
