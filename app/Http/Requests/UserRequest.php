<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name'                  =>  'required',
            'user_name'             =>  ['required', Rule::unique('users')->ignore($this->id)],
            'official_id'           =>  'required',
            'department_id'         =>  'required',
            'designation_id'        =>  'required',
            'user_type'             =>  'required',
            'email'                 =>  ['required', Rule::unique('users')->ignore($this->id)],
            'check_pass_change'     =>  ['required'],
        ];

        if($this->check_pass_change == 2){
            $rules['password']              =  'required|alpha_num|between:6,12';
            $rules['password_confirmation'] =  'same:password|required|between:6,12';

        }

        return $rules;
    }

    public function messages()
    {
        return [
            'username.required' => 'Username is required!',
            'password.required' => 'Password is required!',
        ];
    }
}
