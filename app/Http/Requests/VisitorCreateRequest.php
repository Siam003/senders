<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class VisitorCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'        =>  'required',
            'mobile_no'         =>  'required',
            'email'             =>  ['required', Rule::unique('visitor_info')->ignore($this->id)],
            'nid'               =>  'required',
            'visitor_company'   =>  'required',
            'first_name'        =>  'required',
        ];
    }
}
