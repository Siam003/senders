<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitorSchedule extends Model
{
    protected $table = 'visitor_schedules';

    protected $fillable = [
        'id',
        'visitor_id',
        'schedule_no',
        'purpose',
        'has_material',
        'has_vahical',
        'vehical_no',
        'visit_type',
        'visit_with',
        'status',
        'visit_date',
        'scheduled_time',
        'entry_time',
        'exit_time',
        'created_by',
        'approved_by',
        'reviewed_by',
        'created_at',
        'updated_at',
    ];

    const DRAFT_STATUS      =   1;
    const CREATED_STATUS    =   2;
    const REVIEWD_STATUS    =   3;
    const APPROVED_STATUS   =   4;
    const REJECTED_STATUS   =   5;

    const STATUS = [
        self::DRAFT_STATUS      =>  'Draft',
        self::CREATED_STATUS    =>  'Created',
        self::REVIEWD_STATUS    =>  'Reviewd',
        self::APPROVED_STATUS   =>  'Approved',
        self::REJECTED_STATUS   =>  'Rejected',
    ];

    public function getStatusAttribute($value)
    {

        if ($value == 1) {
            return '<code class="primary-code">Draft</code>';
        }
        if ($value == 2) {
            return '<code class="warning-code">Created</code>';
        }
        if ($value == 3) {
            return '<code class="success-code">Reviewed</code>';
        }
        if ($value == 4) {
            return '<code class="success-code">Approved</code>';
        }
        if ($value == 5) {
            return '<code class="danger-code">Rejected</code>';
        }
    }

    public function materials()
    {
        return $this->hasMany(VisitorsMaterial::class, 'schedule_id', 'id');
    }

    public function visitor()
    {
        return $this->belongsTo(Visitor::class, 'visitor_id', 'id')->withDefault();
    }

    public function visitEmployee()
    {
        return $this->belongsTo(Employee::class, 'visit_with', 'id')->withDefault();
    }
    public function createdBy()
    {
        return $this->belongsTo(User::class,  'created_by', 'id')->withDefault();
    }
    public function approvedBy()
    {
        return $this->belongsTo(User::class,  'approved_by', 'id')->withDefault();
    }
    public function reviewedBy()
    {
        return $this->belongsTo(User::class,  'reviewed_by', 'id')->withDefault();
    }
    public function occurrence()
    {
        return $this->hasMany(VisitorsOccurrence::class,  'schedule_id', 'id');
    }
}
