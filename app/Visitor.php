<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{


    protected $table = 'visitor_info';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'visitor_company',
        'visitor_designation',
        'mobile_no',
        'email',
        'nid',
        'photo',
        'finger_print',
        'created_at',
        'updated_at'
    ];

    public function schedules(){
        return $this->hasMany(VisitorSchedule::class,'visitor_id','id');
    }
}
