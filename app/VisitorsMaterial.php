<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitorsMaterial extends Model
{
    protected $table = 'visitors_materials';

    protected $fillable = [
        'id',
        'visitor_id',
        'schedule_id',
        'material_name',
        'unique_identity_no',
        'manufacturer',
        'quantity',
        'is_returnable',
        'remarks',
        'created_at',
        'updated_at',
    ];
}
