<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';
    protected $fillable = [
        'id',
        'official_id',
        'first_name',
        'last_name',
        'designation',
        'mobile_no',
        'email',
        'nid',
    ];
}
