<?php

namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\Redis;
use App\Repositories\Interfaces\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{

    /**
     * @return userInstance
     */
    public function getModelInstance()
    {
        return new User();
    }

    /**
     * @param mixed $filterArray
     * 
     * @return $results
     */

    public function getAllUser($filterArray)
    {
        $query      =   $this->getModelInstance()->query();
        $users      =   $query->orderBy('id','desc')->paginate();
        return $users;
    }

    public function createUpdate($data)
    {

        $userId = request()->segment(3);
        $model = $this->getModelInstance()->findOrNew($userId);

        $model->name              =     $data->name;
        $model->user_name         =     $data->user_name;
        $model->official_id       =     $data->official_id;
        $model->department_id     =     $data->department_id;
        $model->designation_id    =     $data->designation_id;
        $model->email             =     $data->email;
        $model->mobile_no         =     $data->mobile_no;
        $model->user_type         =     $data->user_type;
        $model->password          =     bcrypt($data->password);

        if ($data->hasFile('photo')) {
            $destinationPath = '/uploads/user/';
            $file = request('photo');
            $filename = $file->getClientOriginalName();
            $file->move(public_path() . $destinationPath, $filename);
            $model->photo = $destinationPath . $filename;
        }
        $model->save();
        return true;
    }

    public function deleteUser($userId)
    {
        $model = $this->getModelInstance()->find($userId);
        $model->delete();
        return true;
    }
}
