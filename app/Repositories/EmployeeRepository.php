<?php

namespace App\Repositories;

use App\Employee;
use App\Repositories\Interfaces\EmployeeRepositoryInterface;

class EmployeeRepository implements EmployeeRepositoryInterface
{

    /**
     * @return userInstance
     */
    public function getModelInstance()
    {
        return new Employee();
    }

    public function getAllEmployees($filterArray)
    {
        $query      =   $this->getModelInstance()->query();
        $visitors      =   $query->paginate();
        return $visitors;
    }


    public function createUpdate($data)
    {
    }
}
