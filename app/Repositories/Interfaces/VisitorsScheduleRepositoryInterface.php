<?php

namespace App\Repositories\Interfaces;

interface VisitorsScheduleRepositoryInterface
{
    /**
     * @return getModelInstance
     */

    public function getModelInstance();

    public function createUpdate($data);

    public function getAllSchedules($filterArray);

    public function makeReviewd($scheduleId);

    public function makeReject($scheduleId);

    public function makeApproved($scheduleId);

    public function getDataByDate($date);

    public function getTotalByDateStatus($date,$status);

    public function getAllScheduleData($filterArray);

    public function generateSearchQuery($filterArray);

}
