<?php

namespace App\Repositories\Interfaces;

interface VisitorsOccurrenceRepositoryInterface
{
    /**
     * @return getModelInstance
     */

    public function getModelInstance();

    public function createUpdate($data);
}
