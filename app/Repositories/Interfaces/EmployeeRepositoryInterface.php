<?php

namespace App\Repositories\Interfaces;

interface EmployeeRepositoryInterface
{
    /**
     * @return getModelInstance
     */

    public function getModelInstance();

    public function createUpdate($data);

    public function getAllEmployees($filterArray);
}
