<?php

namespace App\Repositories\Interfaces;

interface VisitorRepositoryInterface
{
    /**
     * @return getModelInstance
     */

    public function getModelInstance();

    public function createUpdate($data);

    public function getAllVisitors($filterArray);

}
