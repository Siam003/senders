<?php

namespace App\Repositories\Interfaces;

interface VisitorsMaterialRepositoryInterface
{

    public function getModelInstance();
    public function createUpdate($data);
    public function createBulk($data, $schedule);
    public function getAllVisitorsMaterial($filterArray);
    public function saveReturnMaterial($data);
}
