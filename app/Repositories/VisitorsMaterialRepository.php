<?php

namespace App\Repositories;


use App\Repositories\Interfaces\VisitorsMaterialRepositoryInterface;
use App\VisitorsMaterial;

class VisitorsMaterialRepository implements VisitorsMaterialRepositoryInterface
{
    public function getModelInstance()
    {
        return new VisitorsMaterial();
    }

    public function createUpdate($data)
    {
    }

    public function createBulk($data, $schedule)
    {
        $materialArray = [];


        foreach ($data->material_name as $key => $value) {
            $materialArray[$key]['visitor_id']          =   $data->visitor_id;
            $materialArray[$key]['schedule_id']         =   $schedule->id;
            $materialArray[$key]['material_name']       =   $data->material_name[$key];
            $materialArray[$key]['serial_no']           =   $data->serial_no[$key];
            $materialArray[$key]['description']         =   $data->description[$key];
            $materialArray[$key]['quantity']            =   $data->quantity[$key];
            $materialArray[$key]['is_returnable']       =   $data->is_returnable[$key];
        }

        $this->getModelInstance()->insert($materialArray);
        return true;
    }

    public function getAllVisitorsMaterial($filterArray)
    {
    }

    public function saveReturnMaterial($data)
    {
        foreach ($data['material_id'] as $key => $value) {
            $material = $this->getModelInstance()->find($data['material_id'][$key]);
            $material->is_returned      =   $data['is_returned'][$key];
            $material->return_quantity  =   $data['return_quantity'][$key];
            $material->remarks          =   $data['remarks'][$key];
            $material->save();
        }
        return true;
    }
}
