<?php

namespace App\Repositories;


use App\Visitor;
use Carbon\Carbon;
use App\VisitorSchedule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query\Builder;
use App\Repositories\Interfaces\VisitorsScheduleRepositoryInterface;

class VisitorsScheduleRepository implements VisitorsScheduleRepositoryInterface
{

    /**
     * @return userInstance
     */
    public function getModelInstance()
    {
        return new VisitorSchedule();
    }

    public function getAllSchedules($filterArray)
    {
        $query          =   $this->getModelInstance()->query();

        if (!empty($filterArray)) {
            if ($filterArray['visitor_id']) {
                $query  = $query->where('visitor_id', $filterArray['visitor_id']);
            }
            if ($filterArray['visit_with']) {
                $query  = $query->where('visit_with', $filterArray['visit_with']);
            }
            if ($filterArray['status']) {
                $query  = $query->where('status', $filterArray['status']);
            }
            if ($filterArray['date']) {
                $query  = $query->whereDate('visit_date', $filterArray['date']);
            }
            if ($filterArray['schedule_no']) {
                $query  = $query->where('schedule_no', $filterArray['schedule_no']);
            }
        }


        $visitors       =   $query->with('visitor', 'visitEmployee', 'createdBy', 'approvedBy', 'reviewedBy')->paginate();
        return $visitors;
    }

    public function createUpdate($data)
    {
        $scheduleId = request()->id;
        $model = $this->getModelInstance()->findOrNew($scheduleId);

        if (!$scheduleId || $model->getOriginal('status') == 1) {
            $model->status              =     $data->submit_type == 'draft' ? 1 : 2;
        }

        $model->visitor_id          =     $data->visitor_id;
        $model->schedule_no         =     $data->schedule_no;
        $model->purpose             =     $data->purpose;
        $model->visit_type          =     $data->visit_type;
        $model->visit_with          =     $data->visit_with;
        $model->visit_date          =     date('Y-m-d', strtotime($data->visit_date));
        $model->scheduled_time      =     date('H:i', strtotime($data->scheduled_time));
        $model->entry_time          =     $data->entry_time ? date('H:i', strtotime($data->entry_time)) : null;
        $model->exit_time           =     $data->exit_time ? date('H:i', strtotime($data->exit_time)) : null;
        $model->has_material        =     $data->has_material;
        $model->has_vahical         =     $data->has_vahical;
        $model->vehical_no          =     $data->vehical_no;
        $model->created_by          =     Auth::user()->id;
        $model->save();
        return $model;
    }

    public function makeReviewd($scheduleId)
    {
        $model = $this->getModelInstance()->find($scheduleId);
        $model->status = VisitorSchedule::REVIEWD_STATUS;
        $model->reviewed_by = Auth::user()->id;
        $model->save();
        return true;
    }
    public function makeReject($scheduleId)
    {
        $model = $this->getModelInstance()->find($scheduleId);
        $model->status = VisitorSchedule::REJECTED_STATUS;
        $model->reviewed_by = Auth::user()->id;
        $model->save();
        return true;
    }
    public function makeApproved($scheduleId)
    {
        $model = $this->getModelInstance()->find($scheduleId);
        $model->status = VisitorSchedule::APPROVED_STATUS;
        $model->approved_by = Auth::user()->id;
        $model->save();
        return true;
    }

    public function getDataByDate($date)
    {
        $model = $this->getModelInstance()->with('visitor', 'visitEmployee', 'createdBy', 'approvedBy', 'reviewedBy')
            ->where('visit_date', $date)
            ->where('status', VisitorSchedule::APPROVED_STATUS)
            ->get();
        return  $model;
    }

    public function getTotalByDateStatus($date, $status)
    {
        $query = $this->getModelInstance()->with('visitor', 'visitEmployee', 'createdBy', 'approvedBy', 'reviewedBy')->where('visit_date', $date);
        if ($status == 'in') {
            $query = $query->where('entry_time', '!=', null);
            $query = $query->where('exit_time', null);
        }
        if ($status == 'out') {
            $query = $query->where('exit_time', '<>', null);
        }
        $query = $query->where('status', VisitorSchedule::APPROVED_STATUS)->get();
        return  $query;
    }

    public function getAllScheduleData($filterArray)
    {
        if (!empty($filterArray)) {
            $query          =   $this->getModelInstance()->query();

            if ($filterArray['from_date'] && $filterArray['to_date']) {
                $query  = $query->whereDate('visit_date', '>=', $filterArray['from_date'])
                    ->whereDate('visit_date', '<=', $filterArray['to_date']);
            }

            if ($filterArray['status']) {
                $query  = $query->where('status', $filterArray['status']);
            }

            if ($filterArray['name']) {
                $query  = $query->whereHas('visitor', function ($q) use ($filterArray) {
                    $q->where('first_name', 'like', '%' . $filterArray['name'] . '%')
                        ->orWhere('last_name', 'like', '%' . $filterArray['name'] . '%');
                });
            }

            if ($filterArray['visitor_company']) {
                $query  = $query->whereHas('visitor', function ($q) use ($filterArray) {
                    $q->where('visitor_company', 'like', '%' . $filterArray['visitor_company'] . '%');
                });
            }

            if ($filterArray['department_id']) {
                $query  = $query->whereHas('visitEmployee', function ($q) use ($filterArray) {
                    $q->where('department_id', $filterArray['department_id']);
                });
            }

            if ($filterArray['mobile_no']) {
                $query  = $query->whereHas('visitor', function ($q) use ($filterArray) {
                    $q->where('mobile_no', $filterArray['mobile_no']);
                });
            }

            $query       =   $query->with('visitor', 'visitEmployee', 'occurrence')->orderBy('visit_date', 'ASC');
            
            if (request()->segment(2) == 'visitor-history-list-download') {
                $visitors =    $query->get();
            } else {
                $visitors = $query->paginate();
            }
            return $visitors;
        }
        return [];
    }

    public function generateSearchQuery($filterArray){
        if (!empty($filterArray)) {
            $query          =   $this->getModelInstance()->query();


        if (!empty($filterArray)) {
            if ($filterArray['visitor_id']) {
                $query  = $query->where('visitor_id', $filterArray['visitor_id']);
            }
            if ($filterArray['visit_with']) {
                $query  = $query->where('visit_with', $filterArray['visit_with']);
            }
            if ($filterArray['status']) {
                $query  = $query->where('status', $filterArray['status']);
            }
            if ($filterArray['date']) {
                $query  = $query->whereDate('visit_date', $filterArray['date']);
            }
            if ($filterArray['schedule_no']) {
                $query  = $query->where('schedule_no', $filterArray['schedule_no']);
            }
        }

            $query       =   $query->with('visitor', 'visitEmployee', 'occurrence')->orderBy('visit_date', 'ASC');
            
  
            return $query;
        }
    }
}
