<?php

namespace App\Repositories;

use App\Employee;
use App\VisitorsOccurrence;
use App\Repositories\Interfaces\EmployeeRepositoryInterface;
use App\Repositories\Interfaces\OccuranceRepositoryInterface;
use App\Repositories\Interfaces\VisitorsOccurrenceRepositoryInterface;

class VisitorsOccurrenceRepository implements VisitorsOccurrenceRepositoryInterface
{

    /**
     * @return userInstance
     */
    public function getModelInstance()
    {
        return new VisitorsOccurrence();
    }

    public function createUpdate($data)
    {
        $occurrence                     =   $this->getModelInstance();
        $occurrence->schedule_id        =   $data['schedule_id'];
        $occurrence->occurance_type     =   $data['occurance_type'];
        $occurrence->occurance_details  =   $data['occurance_details'];
        $result = $occurrence->save();
        return true;
    }
}
