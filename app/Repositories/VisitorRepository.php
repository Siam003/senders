<?php

namespace App\Repositories;

use App\User;
use App\Visitor;
use App\VisitorSchedule;
use Illuminate\Support\Facades\Redis;
use App\Repositories\Interfaces\VisitorRepositoryInterface;

class VisitorRepository implements VisitorRepositoryInterface
{

    /**
     * @return userInstance
     */
    public function getModelInstance()
    {
        return new Visitor();
    }

    public function getAllVisitors($filterArray)
    {
        $query          =   $this->getModelInstance()->query();
        if (!empty($filterArray)) {
            $query = $query->where($filterArray['search_params'], 'LIKE', "%{$filterArray['keyword']}%");
        }
        $visitors       =   $query->orderBy('id', 'desc')->paginate();
        return $visitors;
    }


    public function createUpdate($data)
    {

        $userId = request()->segment(3);
        $model = $this->getModelInstance()->findOrNew($userId);

        $model->first_name              =     $data->first_name;
        $model->last_name               =     $data->last_name;
        $model->visitor_company         =     $data->visitor_company;
        $model->visitor_designation     =     $data->visitor_designation;
        $model->mobile_no               =     $data->mobile_no;
        $model->email                   =     $data->email;
        $model->nid                     =     $data->nid;
        $model->photo                   =     $data->photo;
        $model->finger_print            =     $data->finger_print;
        $model->save();
        return $model;
    }

    
}
