<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const SUPER_ADMIN   =   'Super Admin';
    const GATEMAN       =   'Gate man';
    const GRADE_ONE     =   'CE/SE/SE'; // approver
    const GRADE_TWO     =   'SDE/AE/SAE'; // reviewer

    const USER_TYPE = [
        0     =>  self::SUPER_ADMIN,
        1     =>  self::GRADE_ONE,
        2     =>  self::GRADE_TWO,
        3     =>  self::GATEMAN,
    ];

    const DEPARTMENTS = [
        0   =>  'Super User',
        1   =>  'I&C',
        2   =>  'EMD',
        3   =>  'MMD',
        4   =>  'OPN',
        5   =>  'HR',
        6   =>  'ACC',
    ];

    const DESIGNATION = [
        0   =>  'Super User',
        1   =>  'CE',
        2   =>  'SE',
        3   =>  'SDE',
        4   =>  'AE',
        5   =>  'SAE',
        6   =>  'GATEMAN',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'official_id',
        'designation_id',
        'department_id',
        'name',
        'user_name',
        'password',
        'email',
        'mobile_no',
        'user_type',
        'photo',
        'created_at',
        'updated_at'
    ];
}
